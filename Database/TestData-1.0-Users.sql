USE [DB_A4653A_cp]
GO

INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'3BBB16F5-1775-4A4B-8BD8-39365FFC38EA', N'rafal.osieka+10@gmail.com', 1, N'ANsl1lyj6O3IbkXUoWtDRifqZ7jlwx2mc6NHa/6+0JByOeKyu4gvoS4+aXasNsK1Pg==', N'E7F8F895-FFFC-41E3-A100-C4361E3518F2', NULL, 0, 0, CAST(N'1754-01-01T00:00:00.000' AS DateTime), 0, 0, N'SuperAdmin')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'44686C10-419E-481D-839B-1E8C6F4A5BC8', N'rafal.osieka+12@gmail.com', 1, N'AMag5FA66vC/bKRvG+RszNQSIZBL+hRq25lG040ZStrvjhSMgx2LTADcWu9IIwVy4g==', N'6DED0251-6C27-4FFD-9140-E45D52FC296A', NULL, 0, 0, CAST(N'1754-01-01T00:00:00.000' AS DateTime), 0, 0, N'User')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'9E65D46D-2405-4074-B0CF-D4E0669398AD', N'rafal.osieka+11@gmail.com', 1, N'ALWNWEreJrJ0KTaapLX0/kAY65jIWJhunH6g7CS50iNMWOMHisQu67R3jUZ7rHxAyA==', N'F16ACCC4-F062-47C1-8CAF-62A27A1A837E', NULL, 0, 0, CAST(N'1754-01-01T00:00:00.000' AS DateTime), 0, 0, N'TrustedUser')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3BBB16F5-1775-4A4B-8BD8-39365FFC38EA', N'53F0060F-CF03-4D41-92E4-E0AB7AFB12F7')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'44686C10-419E-481D-839B-1E8C6F4A5BC8', N'5D7F6D9E-265B-42F6-9966-D4F68C135FC5')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9E65D46D-2405-4074-B0CF-D4E0669398AD', N'7BE144B7-2031-4643-ABD3-407B7480B9C7')
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserId], [Name], [IsActive]) VALUES (1, N'SuperAdmin', 1)
INSERT [dbo].[Users] ([UserId], [Name], [IsActive]) VALUES (2, N'TrustedUser', 1)
INSERT [dbo].[Users] ([UserId], [Name], [IsActive]) VALUES (3, N'User', 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
INSERT [dbo].[UserAspNetUsers] ([UserId], [AspNetUserId]) VALUES (1, N'3BBB16F5-1775-4A4B-8BD8-39365FFC38EA')
INSERT [dbo].[UserAspNetUsers] ([UserId], [AspNetUserId]) VALUES (2, N'9E65D46D-2405-4074-B0CF-D4E0669398AD')
INSERT [dbo].[UserAspNetUsers] ([UserId], [AspNetUserId]) VALUES (3, N'44686C10-419E-481D-839B-1E8C6F4A5BC8')
SET IDENTITY_INSERT [dbo].[FuelNames] ON 

INSERT [dbo].[FuelNames] ([FuelNameId], [Name]) VALUES (1, N'Benzyna bezołowiowa 95')
INSERT [dbo].[FuelNames] ([FuelNameId], [Name]) VALUES (2, N'Benzyna bezołowiowa 98')
INSERT [dbo].[FuelNames] ([FuelNameId], [Name]) VALUES (3, N'VERVA 98 - paliwo premium')
INSERT [dbo].[FuelNames] ([FuelNameId], [Name]) VALUES (4, N'Olej napędowy Ekodiesel ULTRA')
INSERT [dbo].[FuelNames] ([FuelNameId], [Name]) VALUES (5, N'Olej napędowy Ekodiesel ULTRA klasa 2')
INSERT [dbo].[FuelNames] ([FuelNameId], [Name]) VALUES (6, N'VERVA ON - paliwo premium')
INSERT [dbo].[FuelNames] ([FuelNameId], [Name]) VALUES (7, N'BIO 100')
SET IDENTITY_INSERT [dbo].[FuelNames] OFF
SET IDENTITY_INSERT [dbo].[Stations] ON 

INSERT [dbo].[Stations] ([StationId], [Name], [Description], [IsActive], [IsApproved], [Longitude], [Latitude], [AddedByUserId], [ApprovedByUserId]) VALUES (1002, N'Viwa', N'petrol station, shop and car wash', 1, 1, CAST(17.904647 AS Decimal(9, 6)), CAST(50.655990 AS Decimal(9, 6)), 1, NULL)
INSERT [dbo].[Stations] ([StationId], [Name], [Description], [IsActive], [IsApproved], [Longitude], [Latitude], [AddedByUserId], [ApprovedByUserId]) VALUES (1003, N'ORLEN', N'Petrol Station', 1, 1, CAST(17.954266 AS Decimal(9, 6)), CAST(50.469394 AS Decimal(9, 6)), 1, NULL)
INSERT [dbo].[Stations] ([StationId], [Name], [Description], [IsActive], [IsApproved], [Longitude], [Latitude], [AddedByUserId], [ApprovedByUserId]) VALUES (1006, N'Lotos', N'w Krapkowicach', 1, 1, CAST(17.972862 AS Decimal(9, 6)), CAST(50.464106 AS Decimal(9, 6)), 2, NULL)
INSERT [dbo].[Stations] ([StationId], [Name], [Description], [IsActive], [IsApproved], [Longitude], [Latitude], [AddedByUserId], [ApprovedByUserId]) VALUES (1007, N'Wieża ciśnień', N'not approved', 0, 0, CAST(17.957453 AS Decimal(9, 6)), CAST(50.467262 AS Decimal(9, 6)), 3, NULL)
INSERT [dbo].[Stations] ([StationId], [Name], [Description], [IsActive], [IsApproved], [Longitude], [Latitude], [AddedByUserId], [ApprovedByUserId]) VALUES (1008, N'Lotos', N'przy autostradzie', 1, 1, CAST(17.918445 AS Decimal(9, 6)), CAST(50.523696 AS Decimal(9, 6)), 3, 1)
SET IDENTITY_INSERT [dbo].[Stations] OFF
