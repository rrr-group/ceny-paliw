﻿namespace CP.Services.Model
{
    public class Station
    {
        public int StationId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public bool IsApproved { get; set; }

        public decimal Longitude { get; set; }

        public decimal Latitude { get; set; }

        public string AddedByUser { get; set; }

        public string ApprovedByUser { get; set; }
    }
}
