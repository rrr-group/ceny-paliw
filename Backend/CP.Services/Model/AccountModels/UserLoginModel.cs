﻿namespace CP.Services.Model
{
    #region Usings
    using System.ComponentModel.DataAnnotations;
    #endregion
    public class UserLoginModel
    {
        #region Properties
        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nazwa użytkownika jest wymagane.")]
        [Display(Name = "Login:")]
        public string Login { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Hasło jest wymagane.")]
        [Display(Name = "Hasło:")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        #endregion
    }
}
