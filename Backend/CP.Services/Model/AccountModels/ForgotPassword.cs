﻿namespace CP.Services.Model
{
    public class ForgotPassword
    {
        #region Properties
        public string Email { get; set; }
        #endregion
    }
}
