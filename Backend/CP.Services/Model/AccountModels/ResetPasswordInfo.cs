﻿namespace CP.Services.Model
{
    #region Usings
    using System.ComponentModel.DataAnnotations;
    #endregion

    public class ResetPasswordInfo
    {
        #region Properties
        public string UserId { get; set; }

        public string Token { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole hasło jest wymagane.")]
        [StringLength(50, ErrorMessage = "Maksymalna długość hasła to 50 znaków.")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole potwierdź hasło jest wymagane.")]
        [StringLength(50, ErrorMessage = "Maksymalna długość hasła to 50 znaków.")]
        [Compare("Password", ErrorMessage = "Hasła nie zgadzają się.")]
        public string ConfirmPassword { get; set; }
        #endregion
    }
}
