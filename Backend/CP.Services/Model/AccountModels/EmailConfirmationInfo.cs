﻿namespace CP.Services.Model
{
    public class EmailConfirmationInfo
    {
        #region Properties
        public string UserId { get; set; }

        public string Token { get; set; }
        #endregion
    }
}
