﻿namespace CP.Services.Model
{
    #region Usings
    using System.ComponentModel.DataAnnotations;
    #endregion
    public class UserRegistrationInfo
    {
        #region Properties
        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nazwa użytkownika jest wymagane.")]
        [StringLength(50, ErrorMessage = "Maksymalna długośc nazwy użytkownika to 50 znaków.")]
        [RegularExpression(@"^[a-zA-Z0-9.-]+$", ErrorMessage = "Dozwolone znaki w polu nazwa użytkownika: A-Z, 0-9, \".\", \"-\".")]
        public string Login { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole hasło jest wymagane.")]
        [StringLength(50, ErrorMessage = "Maksymalna długośc hasła to 50 znaków.")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole potwierdź hasło jest wymagane.")]
        [StringLength(50, ErrorMessage = "Maksymalna długośc hasła to 50 znaków.")]
        [Compare("Password", ErrorMessage = "Hasła nie zgadzają się.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Adres email nie jest poprawny.")]
        [StringLength(50, ErrorMessage = "Maksymalna długośc emaila to 50 znaków.")]
        public string Email { get; set; }
        #endregion
    }
}
