﻿namespace CP.Services.Model
{
    public class FuelName
    {
        public int FuelNameId { get; set; }

        public string Name { get; set; }
    }
}
