﻿namespace CP.Services.Model
{
    public class FuelDetails
    {
        public int FuelId { get; set; }

        public int FuelNameId { get; set; }

        public string FuelName { get; set; }

        public System.DateTime Date { get; set; }

        public bool IsApproved { get; set; }

        public decimal Price { get; set; }
    }
}
