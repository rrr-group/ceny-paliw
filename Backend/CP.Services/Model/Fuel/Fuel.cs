﻿namespace CP.Services.Model
{
    public class Fuel
    {
        public int FuelId { get; set; }

        public int FuelNameId { get; set; }

        public int StationId { get; set; }

        public decimal Price { get; set; }
    }
}
