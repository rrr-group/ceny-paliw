﻿namespace CP.Services.Model
{
    public class StationDto
    {
        public string Name { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }
}
