﻿namespace CP.Services.Model
{
    public class StationFilters
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public int Radius { get; set; }
    }
}
