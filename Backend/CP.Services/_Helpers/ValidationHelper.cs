﻿namespace CP.Services._Helpers
{
    #region Usings
    using System;
    using System.Diagnostics;
    using Common;
    #endregion

    public static class ValidationHelper
    {
        #region Methods
        public static void IsFound(object subject, string subjectName, int subjectId, string guid)
        {
            IsFound(subject, subjectName, subjectId.ToString(), guid);
        }

        public static void IsFound(object subject, string subjectName, string subjectId, string guid)
        {
            if (subject == null)
            {
                throw new NotFoundException($"Any '{subjectName}' for id = {subjectId} not found.", guid, ErrorCode.ResourceNotFound);
            }
        }

        public static void IsModelNotNull(object subject, string subjectName, string guid)
        {
            if (subject == null)
            {
                throw new BadRequestException($"The input '{subjectName}' does not exist.", guid, ErrorCode.ModelNotFound);
            }
        }

        public static void AreIdsMatching(int id1, int id2, string subjectName, string guid)
        {
            if (id1 != id2)
            {
                throw new BadRequestException($"The id does not match for the input '{subjectName}'.", guid);
            }
        }

        public static void IsArgumentNotNull(object subject, string subjectName)
        {
            if (subject == null)
            {
                throw new ArgumentNullException(subjectName);
            }
        }
        #endregion
    }
}
