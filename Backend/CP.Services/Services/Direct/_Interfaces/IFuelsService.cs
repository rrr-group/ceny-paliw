﻿namespace CP.Services.Services.Direct
{
    #region Usings
    using CP.Services.Model;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    #endregion

    public interface IFuelsService
    {
        Task<FuelDetails> Add(Fuel fuelFromPost);

        Task<IEnumerable<FuelDetails>> GetFuelsByStationId(int stationId);

        Task<IEnumerable<FuelName>> GetFuelNames();
    }
}
