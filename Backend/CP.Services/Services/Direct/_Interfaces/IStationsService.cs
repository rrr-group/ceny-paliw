﻿namespace CP.Services.Services.Direct
{
    #region Usings
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using CP.Services.Model;
    #endregion

    public interface IStationsService
    {
        Task<IEnumerable<Station>> GetAllStations();

        Task<IEnumerable<Station>> GetFilteredStations(StationFilters filters);

        Task<Station> GetStationById(int stationId);

        Task<Station> Add(StationDto station);
    }
}
