﻿namespace CP.Services.Services.Direct
{
    #region Usings
    using CP.Common;
    using CP.DataAccess.Factory;
    #endregion

    public class UsersService : ServiceBase, IUsersService
    {
        public UsersService(IDatabaseContextFactory databaseContextFactory, IAuthorizationDataProvider authorizationDataProvider)
            : base(databaseContextFactory, authorizationDataProvider)
        {
        }
    }
}
