﻿namespace CP.Services.Services.Direct
{
    #region Usings
    using AutoMapper;
    using CP.Common;
    using CP.DataAccess.Factory;
    using CP.Services._Helpers;
    using CP.Services.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    #endregion

    public class FuelsService : ServiceBaseWithMapper, IFuelsService
    {
        #region Constructors
        public FuelsService(IDatabaseContextFactory databaseContextFactory, IMapper mapper, IAuthorizationDataProvider authorizationDataProvider)
            : base(databaseContextFactory, mapper, authorizationDataProvider)
        {
        }
        #endregion

        #region Methods
        public async Task<FuelDetails> Add(Fuel fuelFromPost)
        {
            ValidationHelper.IsModelNotNull(fuelFromPost, "fuel", "{0AFA42FF-EB63-494E-9259-2AADC4E776B6}");
            using (var databaseContext = this.DatabaseContextFactory.GetDatabaseContext())
            {
                var addedByUser = await GetAuthorizedUser(databaseContext, "{8BA8693F-4662-4BB1-AE27-80D2C9C11F30}");
                var fuelName = await databaseContext.FuelNames.SingleOrDefaultAsync(f => f.FuelNameId == fuelFromPost.FuelNameId);
                var station = await databaseContext.Stations.SingleOrDefaultAsync(s => s.StationId == fuelFromPost.StationId);
                var isUserNotTrusted = IsUserNotTrusted(addedByUser);
                var fuelToAdd = new DataAccess.Model.Fuel()
                {
                    Price = fuelFromPost.Price,
                    Date = DateTime.Now,
                    IsApproved = !isUserNotTrusted
                };

                fuelToAdd.FuelName = fuelName ?? throw new BadRequestException("FuelName does not exists.", "{32EB2D4A-3C13-4123-B8EB-81C3690DD47B}", ErrorCode.FuelNameNotExists);
                fuelToAdd.Station = station ?? throw new BadRequestException("Station does not exists.", "{C9D7BDC7-140D-4015-9F89-54C75EACB03B}", ErrorCode.StationNotExists);
                fuelToAdd.AddedByUser = addedByUser;

                databaseContext.Fuels.Add(fuelToAdd);
                await this.SaveToDatabase(databaseContext);

                return this.Mapper.Map<FuelDetails>(fuelToAdd);
            }
        }

        public async Task<IEnumerable<FuelDetails>> GetFuelsByStationId(int stationId)
        {
            using (var databaseContext = this.DatabaseContextFactory.GetDatabaseContext())
            {
                if (!databaseContext.Stations.Any(station => station.StationId == stationId))
                {
                    throw new BadRequestException($"Station(id: {stationId}) does not exists", "{9CED51CA-C343-4421-AB91-708D4EB59BCD}", ErrorCode.StationNotExists);
                }

                var fuels = await databaseContext.Fuels
                    .Where(fuel => fuel.StationId == stationId)
                    .GroupBy(fuel => fuel.FuelNameId)
                    .Select(group => group.OrderByDescending(fuel => fuel.Date))
                    .ToListAsync();

                var result = new List<FuelDetails>();

                fuels.ForEach(fuelGroup =>
                {
                    result.Add(this.Mapper.Map<FuelDetails>(fuelGroup.FirstOrDefault()));
                });

                return result;
            }
        }

        public async Task<IEnumerable<FuelName>> GetFuelNames()
        {
            using (var databaseContext = this.DatabaseContextFactory.GetDatabaseContext())
            {
                var fuelNames = await databaseContext.FuelNames.ToListAsync();
                return fuelNames.Select(fuelName => this.Mapper.Map<FuelName>(fuelName));
            }
        }
        #endregion
    }
}
