﻿namespace CP.Services.Services.Direct
{
    #region Usings
    using AutoMapper;
    using CP.Common;
    using CP.DataAccess.Factory;
    using CP.Services._Helpers;
    using CP.Services.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Device.Location;
    using System.Linq;
    using System.Threading.Tasks;
    #endregion
    public class StationsService : ServiceBaseWithMapper, IStationsService
    {
        private readonly IFuelsService fuelsService;

        #region Constructors
        public StationsService(IDatabaseContextFactory databaseContextFactory, IMapper mapper, IAuthorizationDataProvider authorizationDataProvider, IFuelsService fuelsService)
            : base(databaseContextFactory, mapper, authorizationDataProvider)
        {
            this.fuelsService = fuelsService;
        }
        #endregion

        #region Methods
        public async Task<IEnumerable<Station>> GetAllStations()
        {
            using (var databaseContext = this.DatabaseContextFactory.GetDatabaseContext())
            {
                var stations = await databaseContext.Stations.ToListAsync();
                return stations.Select(station => this.Mapper.Map<Station>(station));
            }
        }

        public async Task<IEnumerable<Station>> GetFilteredStations(StationFilters filters)
        {
            ValidationHelper.IsModelNotNull(filters, "stationFilter", "{B6A4E4FD-CF4A-423F-BCBD-D5BC2C9E6047}");
            using (var databaseContext = this.DatabaseContextFactory.GetDatabaseContext())
            {
                var stations = await databaseContext.Stations.ToListAsync();

                var geoCoordinate = new GeoCoordinate((double)filters.Latitude, (double)filters.Longitude);
                var radius = (double)filters.Radius;

                var filteredStations = stations.Where(station =>
                {
                    var stationPosition = new GeoCoordinate((double)station.Latitude, (double)station.Longitude);
                    return geoCoordinate.GetDistanceTo(stationPosition) <= radius;
                }).Select(station => this.Mapper.Map<Station>(station));

                return filteredStations;
            }
        }

        public async Task<Station> GetStationById(int stationId)
        {
            using (var databaseContext = this.DatabaseContextFactory.GetDatabaseContext())
            {
                if (databaseContext.Stations.Any(s => s.StationId == stationId) == false)
                {
                    throw new BadRequestException($"Station with id = {stationId} does not exists.", "{AE4A50EB-0552-41ED-9F7B-4BAF9CC2DFB0}", ErrorCode.StationNotExists);
                }

                var station = await databaseContext.Stations.SingleOrDefaultAsync(s => s.StationId == stationId);
                return this.Mapper.Map<Station>(station);
            }
        }

        public async Task<Station> Add(StationDto station)
        {
            ValidationHelper.IsModelNotNull(station, "station", "{520423AE-01C3-4D15-8573-098744B6D0C5}");
            using (var databaseContext = this.DatabaseContextFactory.GetDatabaseContext())
            {
                var addedByUser = await GetAuthorizedUser(databaseContext, "{0CFED327-1725-48AA-9F7C-A01E523230C3}");
                var isUserNotTrusted = IsUserNotTrusted(addedByUser);

                var stationToAdd = new DataAccess.Model.Station()
                {
                    Name = station.Name,
                    Latitude = station.Latitude,
                    Longitude = station.Longitude,
                    AddedByUser = addedByUser,
                    IsActive = true,
                    IsApproved = !isUserNotTrusted,
                    Description = "Adres:N/A;Godziny otwarcia:N/A;Myjnia:N/A;Kompresor:N/A;Odkurzacz:N/A"
                };

                databaseContext.Stations.Add(stationToAdd);
                await this.SaveToDatabase(databaseContext);

                var fuelNames = await databaseContext.FuelNames.ToListAsync();

                var fuels = fuelNames.Select(fuelName => new DataAccess.Model.Fuel()
                {
                    Price = 0,
                    FuelNameId = fuelName.FuelNameId,
                    StationId = stationToAdd.StationId,
                    Date = DateTime.Now,
                    IsApproved = true,
                    AddedByUser = addedByUser
                });

                databaseContext.Fuels.AddRange(fuels);
                await this.SaveToDatabase(databaseContext);

                return this.Mapper.Map<Station>(stationToAdd);
            }
        }
        #endregion
    }
}
