﻿namespace CP.Services.Services.Authentication
{
    #region Usings
    using System.Data.Common;
    using System.Data.Entity.Infrastructure;
    using Microsoft.AspNet.Identity.EntityFramework;
    #endregion

    public class AppIdentityContext : IdentityDbContext<AppIdentityUser>
    {
        #region Constructors
        public AppIdentityContext()
        {
        }

        public AppIdentityContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public AppIdentityContext(string nameOrConnectionString, bool throwIfV1Schema)
            : base(nameOrConnectionString, throwIfV1Schema)
        {
        }

        public AppIdentityContext(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection)
            : base(existingConnection, model, contextOwnsConnection)
        {
        }

        public AppIdentityContext(DbCompiledModel model)
            : base(model)
        {
        }

        public AppIdentityContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
        }

        public AppIdentityContext(string nameOrConnectionString, DbCompiledModel model)
            : base(nameOrConnectionString, model)
        {
        }
        #endregion
    }
}
