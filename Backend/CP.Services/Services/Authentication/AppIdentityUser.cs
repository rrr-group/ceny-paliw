﻿namespace CP.Services.Services.Authentication
{
    #region Usings
    using Microsoft.AspNet.Identity.EntityFramework;
    #endregion

    public class AppIdentityUser : IdentityUser
    {
        #region Constructors
        public AppIdentityUser()
        {
        }
        #endregion
    }
}
