﻿namespace CP.Services.Services.Authentication
{
    #region Usings
    using System;
    using System.Configuration;
    using CP.Services.Services.Messaging;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin;
    #endregion

    public class AppUserManager : UserManager<AppIdentityUser>
    {
        #region Constructors
        public AppUserManager()
            : base(new UserStore<AppIdentityUser>(new AppIdentityContext()))
        {
            UserValidator = new UserValidator<AppIdentityUser>(this)
            {
                AllowOnlyAlphanumericUserNames = false
            };
        }

        public AppUserManager(IUserStore<AppIdentityUser> store)
            : base(store)
        {
            UserValidator = new UserValidator<AppIdentityUser>(this)
            {
                AllowOnlyAlphanumericUserNames = false
            };
        }
        #endregion

        #region Methods
        public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> options, IOwinContext context)
        {
            var manager = new AppUserManager
            {
                PasswordHasher = new SQLPasswordHasher(),
                EmailService = new EmailService()
            };

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                int lifespan = Convert.ToInt32(ConfigurationManager.AppSettings["AccountTokenLifeSpanInMinutes"]);
                manager.UserTokenProvider = new DataProtectorTokenProvider<AppIdentityUser>(dataProtectionProvider.Create("App ASP.NET Identity"))
                {
                    TokenLifespan = TimeSpan.FromMinutes(lifespan)
                };
            }

            return manager;
        }
        #endregion
    }
}
