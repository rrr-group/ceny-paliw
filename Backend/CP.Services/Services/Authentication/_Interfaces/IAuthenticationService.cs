﻿namespace CP.Services.Services.Authentication
{
    #region Usings
    using System.Threading.Tasks;
    using CP.Services.Model;
    using Microsoft.AspNet.Identity;
    #endregion

    public interface IAuthenticationService
    {
        #region Methods
        Task<AppIdentityUser> FindUser(AppUserManager userManager, string login, string password);

        Task RegisterUser(AppUserManager userManager, UserRegistrationInfo userRegistration, string verifyEmailUrlLink);

        Task VerifyEmail(AppUserManager userManager, EmailConfirmationInfo emailConfirmationInfo);

        Task ForgotPassword(AppUserManager userManager, string email);

        Task ResetPassword(AppUserManager userManager, ResetPasswordInfo resetPasswordInfo);
        #endregion
    }
}
