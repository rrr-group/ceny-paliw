﻿namespace CP.Services.Services.Authentication
{
    #region Usings
    using System;
    using System.Configuration;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using CP.Common;
    using CP.DataAccess.Factory;
    using CP.DataAccess.Model;
    using CP.Services._Helpers;
    using CP.Services.Model;
    using Microsoft.AspNet.Identity;
    #endregion

    public class AuthenticationService : ServiceBase, IAuthenticationService
    {
        #region Private variables
        IConfigSettingsProvider configSettingsProvider;
        #endregion

        #region Constructors
        public AuthenticationService(
            IDatabaseContextFactory databaseContextFactory, 
            IAuthorizationDataProvider authorizationDataProvider, 
            IConfigSettingsProvider configSettingsProvider) : base(databaseContextFactory, authorizationDataProvider)
        {
            this.configSettingsProvider = configSettingsProvider;
        }
        #endregion

        #region Methods
        public async Task<AppIdentityUser> FindUser(AppUserManager userManager, string login, string password)
        {
            return await userManager.FindAsync(login, password);
        }

        public async Task RegisterUser(AppUserManager userManager, UserRegistrationInfo userRegistration, string verifyEmailUrlLink)
        {
            ValidationHelper.IsArgumentNotNull(userRegistration, nameof(userRegistration));

            using (var databaseContext = this.DatabaseContextFactory.GetDatabaseContext())
            {
                await this.MyCheckIfEmailIsTaken(userManager, userRegistration.Email);

                await this.MyCreateAspUser(userManager, userRegistration);
                var createdUser = await userManager.FindByNameAsync(userRegistration.Login);

                await userManager.AddToRoleAsync(createdUser.Id, Role.Trusted.ToString());
                await this.MyCreateUser(databaseContext, userManager, createdUser);

                await MyCreateUserSaveDb(databaseContext, userManager, createdUser);

                // await this.MySendUserConfirmationEmail(userManager, createdUser, verifyEmailUrlLink);
            }
        }

        public Task ForgotPassword(AppUserManager userManager, string email)
        {
            throw new NotImplementedException();
        }

        public Task ResetPassword(AppUserManager userManager, ResetPasswordInfo resetPasswordInfo)
        {
            throw new NotImplementedException();
        }

        public async Task VerifyEmail(AppUserManager userManager, EmailConfirmationInfo emailConfirmationInfo)
        {
            ValidationHelper.IsArgumentNotNull(emailConfirmationInfo, nameof(emailConfirmationInfo));
            ValidationHelper.IsArgumentNotNull(emailConfirmationInfo.UserId, nameof(emailConfirmationInfo.UserId));
            ValidationHelper.IsArgumentNotNull(emailConfirmationInfo.Token, nameof(emailConfirmationInfo.Token));

            string token = Encoding.ASCII.GetString(HttpServerUtility.UrlTokenDecode(emailConfirmationInfo.Token));

            await this.MyCheckIfEmailAlreadyActivated(userManager, emailConfirmationInfo.UserId);

            var confirmResult = await userManager.ConfirmEmailAsync(emailConfirmationInfo.UserId, token);
            this.MyHandleIdentityResultError(confirmResult);

            await this.MySetUserAsActive(emailConfirmationInfo.UserId);
        }
        #endregion

        #region My methods
        private async Task MySendUserConfirmationEmail(AppUserManager userManager, AppIdentityUser createdUser, string verifyEmailUrlLink)
        {
            string emailToken = await userManager.GenerateEmailConfirmationTokenAsync(createdUser.Id);

            emailToken = HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(emailToken));
            var userId = HttpUtility.UrlEncode(createdUser.Id);

            var linkPrototype = this.configSettingsProvider.VerifyEmailLink;
            var verifyLink = string.Format(linkPrototype, userId, emailToken);
            var completeVerifyLink = $"<a href={verifyLink} target='_blank'>Kliknij tutaj</a>";

            var subject = "Aktywacja konta w systemie Cen Paliw";
            var body = string.Format(
@"Witaj w systemie Cen Paliw!<br/>
Kliknij poniższy link lub skopiuj go i wklej w oknie przeglądarki, aby aktywować konto:<br/>
{0}<br/>Link będzie ważny przez {1} godzin.", completeVerifyLink, this.configSettingsProvider.AccountTokenLifeSpanInMinutes / 60);
            await userManager.SendEmailAsync(createdUser.Id, subject, body);
        }

        private async Task MyCreateUserSaveDb(IDatabaseContext databaseContext, AppUserManager manager, AppIdentityUser user)
        {
            try
            {
                await SaveToDatabase(databaseContext);
            }
            catch (Exception e)
            {
                await MyCleanUpCreatedUserRecords(databaseContext, manager, user);
                throw e;
            }
        }

        private async Task<AppIdentityUser> MyCreateAspUser(AppUserManager userManager, UserRegistrationInfo userRegistration)
        {
            AppIdentityUser user = new AppIdentityUser
            {
                UserName = userRegistration.Login,
                Email = userRegistration.Email,
                EmailConfirmed = true,
                PasswordHash = userManager.PasswordHasher.HashPassword(userRegistration.Password),
                TwoFactorEnabled = false,
                SecurityStamp = Guid.NewGuid().ToString(),
                LockoutEnabled = false,
            };

            var result = await userManager.CreateAsync(user);
            this.MyHandleIdentityResultError(result);

            return user;
        }

        private async Task MyCreateUser(IDatabaseContext dbContext, AppUserManager userManager, AppIdentityUser identityUser)
        {
            var user = new User()
            {
                Name = identityUser.UserName,
                IsActive = true
            };

            var aspUser = await dbContext.AspNetUsers.SingleOrDefaultAsync(dbUser => dbUser.Id == identityUser.Id);

            user.AspNetUsers.Add(aspUser);
            dbContext.Users.Add(user);

            await MyCreateUserSaveDb(dbContext, userManager, identityUser);
        }

        private async Task MyCleanUpCreatedUserRecords(IDatabaseContext dbContext, AppUserManager userManager, AppIdentityUser identityUser)
        {
            var aspUser = await dbContext.AspNetUsers
                .Include(u => u.Users)
                .SingleOrDefaultAsync(u => u.Id == identityUser.Id);

            var user = aspUser.Users.SingleOrDefault();
            if (user != null)
            {
                dbContext.Users.Remove(user);
            }

            if (aspUser == null) return;

            await userManager.DeleteAsync(identityUser);

            await this.SaveToDatabase(dbContext);
        }

        private async Task MySetUserAsActive(string userId)
        {
            using (var databaseContext = this.DatabaseContextFactory.GetDatabaseContext())
            {
                var aspNetUser = await GetAspNetUser(databaseContext, userId);
                var user = aspNetUser.Users.Single();
                user.IsActive = true;
                await this.SaveToDatabase(databaseContext);
            }
        }

        private async Task MyCheckIfEmailIsTaken(AppUserManager userManager, string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user != null)
            {
                throw new BadRequestException($"Email \"{email}\" is already taken.", "{A09E11E2-FD1D-45FC-BC78-0B79F0CF8975}", ErrorCode.EmailAlreadyTaken);
            }
        }

        private void MyHandleIdentityResultError(IdentityResult result)
        {
            if (result == null)
            {
                throw new OperationFailedException("Internal error.", "{A20D6361-AA14-47FE-AC75-05A67D090023}");
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null && result.Errors.Any())
                {
                    throw new IdentityResultException(result.Errors, "{90D2AEE0-252E-4C4D-8612-BB64483CE5A9}", ErrorCode.IdentityFrameworkFailure);
                }

                throw new BadRequestException("Unknown errors.", "{86898FE5-26B4-4D48-9EE4-1FE23B4A27FF}");
            }
        }

        private async Task MyCheckIfEmailAlreadyActivated(AppUserManager userManager, string userId)
        {
            var user = await userManager.FindByIdAsync(userId);
            if (user != null)
            {
                if (user.EmailConfirmed)
                {
                    throw new BadRequestException($"Email \"{user.Email}\" is already confirmed.", "{FFDD3F89-BF10-41CA-8CC9-E36D22A4D4E8}", ErrorCode.EmailAlreadyConfirmed);
                }
            }
        }
        #endregion
    }
}
