﻿namespace CP.Services.Services.Messaging
{
    #region Usings
    using System;
    using System.Configuration;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    #endregion

    public class EmailService : IIdentityMessageService
    {
        #region Methods
        public async Task SendAsync(IdentityMessage message)
        {
            var host = ConfigurationManager.AppSettings["SmtpServer"];
            var port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPortNumber"]);
            var contactMail = ConfigurationManager.AppSettings["ContactMail"];
            var smtpLogin = ConfigurationManager.AppSettings["SmtpLogin"];
            var smtpPassword = ConfigurationManager.AppSettings["SmtpPassword"];

            using (var smtpClient = new SmtpClient(host, port))
            using (var msg = new MailMessage())
            {
                msg.From = new MailAddress(contactMail);
                msg.To.Add(new MailAddress(message.Destination));
                msg.Subject = message.Subject;
                msg.IsBodyHtml = true;
                msg.Body = message.Body;

                var credentials = new System.Net.NetworkCredential(smtpLogin, smtpPassword);
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = true;
                await smtpClient.SendMailAsync(msg);
            }
        }
        #endregion
    }
}
