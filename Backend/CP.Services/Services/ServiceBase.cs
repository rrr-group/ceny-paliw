﻿namespace CP.Services.Services
{
    #region Usings
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Threading.Tasks;
    using CP.Common;
    using CP.DataAccess.Factory;
    using CP.DataAccess.Model;
    #endregion

    public abstract class ServiceBase
    {
        #region Private variables
        protected IDatabaseContextFactory DatabaseContextFactory { get; set; }
        protected IAuthorizationDataProvider AuthorizationDataProvider { get; set; }
        #endregion

        #region Constructors
        public ServiceBase(IDatabaseContextFactory databaseContextFactory, IAuthorizationDataProvider authorizationDataProvider)
        {
            this.DatabaseContextFactory = databaseContextFactory;
            this.AuthorizationDataProvider = authorizationDataProvider;
        }
        #endregion

        #region Methods
        protected async Task SaveToDatabase(IDatabaseContext databaseContext)
        {
            try
            {
                await databaseContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException exception)
            {
                throw new DatabaseOperationException("Problem with concurrency while updating database.", "{647CA9FE-AB83-4E69-9A99-EC923278007A}", exception);
            }
            catch (DbUpdateException exception)
            {
                throw new DatabaseOperationException("Problem while updating database.", "{9FA864A0-F20D-4216-8373-A5F77A82C46B}", exception);
            }
            catch (DbEntityValidationException exception)
            {
                var message = "Problem while validating entities in database. " + MyGetExceptionDetails(exception);
                throw new DatabaseOperationException(message, "{AD39A51B-24A6-45E2-B15F-7FF71A1C195E}", exception);
            }
            catch (NotSupportedException exception)
            {
                throw new DatabaseOperationException("The operation is not supported.", "{B7969C3E-F596-4FB8-AA42-8A06056BA61B}", exception);
            }
            catch (ObjectDisposedException exception)
            {
                throw new DatabaseOperationException("The object has already been disposed.", "{B8B45641-A1BB-47B7-B336-0F19B8833C71}", exception);
            }
            catch (InvalidOperationException exception)
            {
                throw new DatabaseOperationException("The operation is invalid.", "{0F00ECD6-8D2D-4A4A-A17B-B15E384AFC8F}", exception);
            }
        }

        protected async Task<User> GetAuthorizedUser(IDatabaseContext databaseContext, string guid)
        {
            var authorizedUserId = this.AuthorizationDataProvider.UserId;

            var user = await databaseContext.AspNetUsers.SingleOrDefaultAsync(aspUser => string.Equals(aspUser.Id, authorizedUserId));

            if (user == null && user.Users == null && user.Users.Count() != 1)
            {
                throw new NotFoundException(string.Format("Any user for id = {0} not found.", authorizedUserId), guid);
            }
            
            return user.Users.First();
        }

        protected static async Task<AspNetUser> GetAspNetUser(IDatabaseContext databaseContext, string id)
        {
            var user = await databaseContext.AspNetUsers.SingleOrDefaultAsync(item => string.Equals(item.Id, id));
            if (user == null)
            {
                throw new NotFoundException(string.Format("Any user for id = {0} not found.", id), "{D1679168-C37F-4EDD-A808-1D8B069EBC35}");
            }

            return user;
        }

        protected static bool IsUserSuperAdmin(AspNetUser user)
        {
            return user.AspNetRoles.Any(role => role.Name == Constants.SuperAdminRoleName);
        }

        protected static bool IsUserNotTrusted(AspNetUser user)
        {
            return user.AspNetRoles.Any(role => role.Name != Constants.SuperAdminRoleName && role.Name != Constants.TrustedUserRoleName);
        }

        protected static bool IsUserNotTrusted(User user)
        {
            var aspNetUser = user.AspNetUsers.First();
            return IsUserNotTrusted(aspNetUser);
        }
        #endregion

        #region My methods
        private static string MyGetExceptionDetails(DbEntityValidationException exception)
        {
            if (exception.EntityValidationErrors == null) return string.Empty;

            var errors = exception.EntityValidationErrors.SelectMany(item => item.ValidationErrors);
            var details = "";

            foreach (var loopError in errors)
            {
                details += "\n- " + loopError.PropertyName + ": " + loopError.ErrorMessage;
            }

            return details;
        }
        #endregion
    }
}
