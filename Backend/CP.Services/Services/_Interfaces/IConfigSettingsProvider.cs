﻿namespace CP.Services.Services
{
    public interface IConfigSettingsProvider
    {
        int AccountTokenLifeSpanInMinutes { get; }

        int AccessTokenLifeSpanInMinutes { get; }

        string VerifyEmailLink { get; }

        string SetNewPasswordLink { get; }
    }
}
