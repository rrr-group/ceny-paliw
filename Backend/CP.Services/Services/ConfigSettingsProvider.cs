﻿namespace CP.Services.Services
{
    #region Usings
    using System.Configuration;
    #endregion

    public class ConfigSettingsProvider : IConfigSettingsProvider
    {
        #region Properties
        public int AccountTokenLifeSpanInMinutes => MyGetIntValue("AccountTokenLifeSpanInMinutes", 1440);

        public int AccessTokenLifeSpanInMinutes => MyGetIntValue("AccessTokenLifeSpanInMinutes", 120);

        public string VerifyEmailLink => MyGetStringValue("VerifyEmailLink");

        public string SetNewPasswordLink => MyGetStringValue("SetNewPasswordLink");
        #endregion

        #region My Methods
        private static string MyGetStringValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        private static bool MyGetBoolValue(string key, bool defaultValue = false)
        {
            var value = ConfigurationManager.AppSettings[key];

            if (bool.TryParse(value, out var result))
            {
                return result;
            }

            return defaultValue;
        }

        private static int MyGetIntValue(string key, int defaultValue = 0)
        {
            var value = ConfigurationManager.AppSettings[key];

            if (int.TryParse(value, out var result))
            {
                return result;
            }

            return defaultValue;
        }
        #endregion
    }
}
