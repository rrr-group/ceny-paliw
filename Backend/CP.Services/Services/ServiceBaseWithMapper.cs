﻿namespace CP.Services.Services
{
    #region Usings
    using AutoMapper;
    using CP.Common;
    using CP.DataAccess.Factory;
    #endregion

    public abstract class ServiceBaseWithMapper : ServiceBase
    {
        #region Private variables
        protected IMapper Mapper { get; set; }
        #endregion

        #region Constructors
        public ServiceBaseWithMapper(IDatabaseContextFactory databaseContextFactory, IMapper mapper, IAuthorizationDataProvider authorizationDataProvider)
            : base(databaseContextFactory, authorizationDataProvider)
        {
            this.Mapper = mapper;
        }
        #endregion
    }
}
