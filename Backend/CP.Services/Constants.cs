﻿namespace CP.Services
{
    public static class Constants
    {
        #region Constants
        public const string SuperAdminRoleName = "SuperAdmin";

        public const string TrustedUserRoleName = "Trusted";

        public const string UserRoleName = "User";

        public const int OAuthTokenLifetimeInMonths = 1;
        #endregion
    }
}
