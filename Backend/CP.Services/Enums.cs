﻿namespace CP.Services
{
    public enum Role : int
    {
        User,

        Trusted,

        SuperAdmin
    }
}
