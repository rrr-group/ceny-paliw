﻿namespace CP.Common
{
    #region Usings
    using System;
    #endregion

    public abstract class ExceptionWithGuid : Exception
    {
        #region Properties
        public string Guid { get; set; }
        public ErrorCode ErrorCode { get; set; }
        public string UniqueGuid { get; set; }
        #endregion

        #region Constructors
        public ExceptionWithGuid(string message, string guid, ErrorCode errorCode)
            : base(message)
        {
            this.Guid = guid;

            MyAddErrorCodeExceptionData(errorCode);
            MyAddUniqueGuid();
        }

        public ExceptionWithGuid(Exception innerException, string guid, ErrorCode errorCode)
            : base(innerException.Message, innerException)
        {
            this.Guid = guid;

            MyAddErrorCodeExceptionData(errorCode);
            MyAddUniqueGuid();
        }

        public ExceptionWithGuid(string message, string guid, Exception innerException)
            : base(message, innerException)
        {
            this.Guid = guid;

            MyAddUniqueGuid();
        }
        #endregion

        #region Private methods
        private void MyAddErrorCodeExceptionData(ErrorCode errorCode)
        {
            this.ErrorCode = errorCode;
            this.Data.Add(ExceptionHandlingHelper.ConstExceptionDataErrorCodeKey, errorCode);
        }

        private void MyAddUniqueGuid()
        {
            string exceptionUniqueGuid = System.Guid.NewGuid().ToString();
            this.UniqueGuid = exceptionUniqueGuid;
            this.Data.Add(ExceptionHandlingHelper.ConstExceptionDataGuidKey, exceptionUniqueGuid);
        }
        #endregion
    }
}
