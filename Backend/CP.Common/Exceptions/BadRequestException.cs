﻿namespace CP.Common
{
    #region Usings
    using System;
    #endregion

    public class BadRequestException : ExceptionWithGuid
    {
        #region Constructors
        public BadRequestException(string message, string guid, ErrorCode errorCode = ErrorCode.Undefined)
           : base(message, guid, errorCode)
        {
        }
        #endregion
    }
}
