﻿namespace CP.Common
{
    #region Usings
    using System.Collections.Generic;
    #endregion

    public class IdentityResultException : ExceptionWithGuid
    {
        #region Properties
        public IEnumerable<string> Errors { get; private set; }
        #endregion

        #region Contructor
        public IdentityResultException(IEnumerable<string> erorrs, string guid, ErrorCode errorCode = ErrorCode.Undefined)
            : base("Multiple errors.", guid, errorCode)
        {
            this.Errors = erorrs;
            this.Data.Add(ExceptionHandlingHelper.ConstExceptionDataIdentityErrorsKey, this.Errors);
        }
        #endregion
    }
}
