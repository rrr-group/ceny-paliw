﻿namespace CP.Common
{
    #region Usings
    using System.Collections.Generic;
    #endregion

    public class ErrorResponseMessage
    {
        #region Properties
        public string Message { get; set; }

        public ErrorCode Code { get; set; }

        public IEnumerable<string> AdditionalErrors { get; set; }

        public System.DateTime Timestamp { get; set; }

        public string Guid { get; set; }
        #endregion
    }
}
