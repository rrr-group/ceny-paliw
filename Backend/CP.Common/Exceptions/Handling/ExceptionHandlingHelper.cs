﻿namespace CP.Common
{
    #region Usings
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    #endregion

    public static class ExceptionHandlingHelper
    {
        #region Public constatnts
        public const string ConstExceptionDataErrorCodeKey = "ErrorCode";
        public const string ConstExceptionDataIdentityErrorsKey = "AdditionalErrors";
        public const string ConstExceptionDataGuidKey = "Guid";
        #endregion

        #region Methods
        public static HttpResponseMessage GenerateHttpErrorResponse(Exception exception, HttpStatusCode statusCode)
        {
            ErrorCode code = ErrorCode.Undefined;
            if (exception.Data.Contains(ConstExceptionDataErrorCodeKey))
            {
                code = (ErrorCode)exception.Data[ConstExceptionDataErrorCodeKey];
            }

            string[] additionalErrors = null;
            if (exception.Data.Contains(ConstExceptionDataIdentityErrorsKey))
            {
                additionalErrors = (string[])exception.Data[ConstExceptionDataIdentityErrorsKey];
            }

            string exceptionGuid = string.Empty;
            if (exception.Data.Contains(ConstExceptionDataGuidKey))
            {
                exceptionGuid = (string)exception.Data[ConstExceptionDataGuidKey];
            }

            var responseMessage = new ErrorResponseMessage()
            {
                Message = $"The server received bad request. {exception.Message}",
                Code = code,
                AdditionalErrors = additionalErrors,
                Timestamp = DateTime.Now,
                Guid = exceptionGuid
            };

            var response = new HttpResponseMessage(statusCode)
            {
                Content = new ObjectContent<ErrorResponseMessage>(responseMessage, new JsonMediaTypeFormatter())
            };

            return response;
        }
        #endregion
    }
}
