﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CP.Common
{
    public enum ErrorCode
    {
        #region Common error codes
        Undefined = 0,
        UnauthorizedSuperAdmin = 1,
        ResourceNotFound = 2,
        ModelNotFound = 3,
        #endregion

        #region Autherntication related error codes
        UserNotFound = 1001,
        EmailAlreadyTaken = 1002,
        EmailAlreadyConfirmed = 1003,
        IdentityFrameworkFailure = 1004,
        #endregion

        #region Station/fuels related error codes
        FuelNameNotExists = 2001,
        StationNotExists = 2002,
        #endregion
    }
}
