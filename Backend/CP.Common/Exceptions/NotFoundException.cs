﻿namespace CP.Common
{
    #region Usings
    using System;
    #endregion

    public class NotFoundException : ExceptionWithGuid
    {
        #region Constructors
        public NotFoundException(string message, string guid, ErrorCode errorCode = ErrorCode.Undefined)
           : base(message, guid, errorCode)
        {
        }
        #endregion
    }
}
