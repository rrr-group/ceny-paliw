﻿namespace CP.Common
{
    #region Usings
    using System;
    #endregion

    public class UnauthorizedException : ExceptionWithGuid
    {
        #region Constructors
        public UnauthorizedException(string message, string guid, ErrorCode errorCode = ErrorCode.Undefined)
           : base(message, guid, errorCode)
        {
        }
        #endregion
    }
}
