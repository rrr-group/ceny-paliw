﻿namespace CP.Common
{
    #region Usings
    using System;
    #endregion

    public class DatabaseOperationException : ExceptionWithGuid
    {
        #region Constructors
        public DatabaseOperationException(string message, string guid, Exception innerException)
            : base(message, guid, innerException)
        {
        }
        #endregion
    }
}
