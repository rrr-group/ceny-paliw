﻿namespace CP.Common
{
    #region Usings
    using System;
    #endregion

    public class OperationFailedException : ExceptionWithGuid
    {
        #region Contructor
        public OperationFailedException(string message, string guid, ErrorCode errorCode = ErrorCode.Undefined)
           : base(message, guid, errorCode)
        {
        }

        public OperationFailedException(Exception innerException, string guid, ErrorCode errorCode = ErrorCode.Undefined)
            : base(innerException, guid, errorCode)
        {
        }
        #endregion
    }
}
