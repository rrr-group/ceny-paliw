﻿namespace CP.Common
{
    #region Usings
    using System.Collections.Generic;
    #endregion

    public interface IAuthorizationDataProvider
    {
        #region Properties
        string UserId { get; set; }

        IEnumerable<string> Roles { get; set; }
        #endregion

        #region Methods
        void Initialize(IDictionary<string, object> requestProperties, bool allowNoAuthData);
        #endregion
    }
}
