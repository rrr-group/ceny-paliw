﻿namespace CP.Common
{
    #region Usings
    using System.Collections.Generic;
    #endregion

    public class AuthorizationDataProvider : IAuthorizationDataProvider
    {
        #region Private Variables
        private bool allowNoData;
        #endregion

        #region Properties
        public string UserId { get; set; }

        public IEnumerable<string> Roles { get; set; }
        #endregion

        #region Methods
        public void Initialize(IDictionary<string, object> requestProperties, bool allowNoAuthData)
        {
            this.allowNoData = allowNoAuthData;

            this.UserId = this.MyRetrieveUserIdFromProperties(requestProperties, AuthenticationConfiguration.UserIdKey, "{112CECA4-738D-46B4-AD35-C447EC67F6DB}");
            this.Roles = this.MyRetrieveRolesFromProperties(requestProperties);
        }
        #endregion

        #region My Methods
        private string MyRetrieveUserIdFromProperties(IDictionary<string, object> requestProperties, string key, string guid)
        {
            object userIdProperty;
            if (!requestProperties.TryGetValue(key, out userIdProperty) && this.allowNoData is false)
            {
                throw new UnauthorizedException($"User ID data not found. Key: {key}.", guid);
            }

            var userId = userIdProperty as string;
            if (string.IsNullOrEmpty(userId) && this.allowNoData is false)
            {
                throw new UnauthorizedException($"Invalid user ID data. Key: {key}.", guid);
            }

            return userId;
        }

        private IEnumerable<string> MyRetrieveRolesFromProperties(IDictionary<string, object> requestProperties)
        {
            object userRoles;
            if (!requestProperties.TryGetValue(AuthenticationConfiguration.UserRolesKey, out userRoles) && this.allowNoData is false)
            {
                throw new UnauthorizedException($"Roles not found.", "{43642019-6328-476B-966E-0A961B25B7A1}");
            }

            var roles = userRoles as IEnumerable<string>;
            if (roles == null && this.allowNoData is false)
            {
                throw new UnauthorizedException($"Invalid user roles.", "{8F2CC2E3-35A7-4587-B017-A8877F572369}");
            }

            return roles;
        }
        #endregion
    }
}
