﻿namespace CP.Common
{
    public static class AuthenticationConfiguration
    {
        #region Private constants
        private const string ConstUserIdKey = "UserID";

        private const string ConstUserRolesKey = "UserRoles";
        #endregion

        #region Properties
        public static string UserIdKey { get { return ConstUserIdKey; } }

        public static string UserRolesKey { get { return ConstUserRolesKey; } }
        #endregion
    }
}
