﻿namespace CP.WebApplication.GlobalErrorHandling
{
    #region Usings
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;
    #endregion

    public class ValidateModelAttribute : ActionFilterAttribute
    {
        #region Methods
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest,
                    actionContext.ModelState);
            }
        }
        #endregion
    }
}