﻿namespace CP.WebApplication.GlobalErrorHandling
{
    #region Usings
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.ExceptionHandling;
    using Common;
    #endregion

    public class FatalErrorExceptionHandler : ExceptionHandler
    {
        #region Methods
        public override void Handle(ExceptionHandlerContext context)
        {
            context.Result = new TextPlainErrorResult()
            {
                Response = ExceptionHandlingHelper.GenerateHttpErrorResponse(context.Exception, HttpStatusCode.InternalServerError)
            };
        }
        #endregion

        #region Nested classes
        private class TextPlainErrorResult : IHttpActionResult
        {
            #region Properties
            public HttpResponseMessage Response { get; set; }
            #endregion

            #region Methods
            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                return Task.FromResult(Response);
            }
            #endregion
        }
        #endregion
    }
}