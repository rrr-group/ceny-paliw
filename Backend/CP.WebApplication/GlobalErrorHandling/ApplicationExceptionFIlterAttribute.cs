﻿namespace CP.WebApplication.GlobalErrorHandling
{
    #region Usings
    using CP.Common;
    using System.Net;
    using System.Web.Http;
    using System.Web.Http.Filters;
    #endregion

    public class ApplicationExceptionFIlterAttribute : ExceptionFilterAttribute
    {
        #region [ExceptionFilterAttribute] methods
        public override void OnException(HttpActionExecutedContext context)
        {
            var exception = context.Exception;
            if (exception is BadRequestException)
            {
                var response = ExceptionHandlingHelper.GenerateHttpErrorResponse(exception, HttpStatusCode.BadRequest);
                response.ReasonPhrase = "BadRequest";

                throw new HttpResponseException(response);
            }
            else if (exception is IdentityResultException)
            {
                var response = ExceptionHandlingHelper.GenerateHttpErrorResponse(exception, HttpStatusCode.BadRequest);
                response.ReasonPhrase = "IdentityError";

                throw new HttpResponseException(response);
            }
            else if (exception is DatabaseOperationException)
            {
                var response = ExceptionHandlingHelper.GenerateHttpErrorResponse(exception, HttpStatusCode.InternalServerError);
                response.ReasonPhrase = "DbUpdate";

                throw new HttpResponseException(response);
            }
            else if (exception is NotFoundException)
            {
                var response = ExceptionHandlingHelper.GenerateHttpErrorResponse(exception, HttpStatusCode.NotFound);
                response.ReasonPhrase = "NotFound";

                throw new HttpResponseException(response);
            }
        }
        #endregion
    }
}