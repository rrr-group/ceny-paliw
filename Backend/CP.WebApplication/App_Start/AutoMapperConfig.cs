﻿namespace CP.WebApplication
{
    #region Usings
    using AutoMapper;
    using CP.Services.Model;
    #endregion

    public static class AutoMapperConfig
    {
        #region Methods
        public static IConfigurationProvider CreateConfiguration()
        {
            return new MapperConfiguration(cfg =>
            {
                MyCreateMapsFromDataAccessToServicesModel(cfg);
                MyCreateMapsFromServicesModelToDataAccess(cfg);
            });
        }
        #endregion

        #region My methods
        private static void MyCreateMapsFromDataAccessToServicesModel(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<DataAccess.Model.Fuel, FuelDetails>().ConvertUsing(source => FuelToFuelDetailsConverter(source));
            cfg.CreateMap<DataAccess.Model.FuelName, FuelName>();
            cfg.CreateMap<DataAccess.Model.Station, Station>().AfterMap(CompleteStationMapping);
        }

        private static void MyCreateMapsFromServicesModelToDataAccess(IMapperConfigurationExpression cfg)
        {
            //cfg.CreateMap<Services.Model., DataAccess.Model.> ();
        }
        #endregion

        #region My converters
        private static FuelDetails FuelToFuelDetailsConverter(DataAccess.Model.Fuel source)
        {
            return new FuelDetails()
            {
                FuelId = source.FuelId,
                FuelNameId = source.FuelNameId,
                Price = source.Price,
                FuelName = source.FuelName.Name,
                IsApproved = source.IsApproved,
                Date = source.Date ?? new System.DateTime(1970, 1, 1)
            };
        }

        private static void CompleteStationMapping(DataAccess.Model.Station source, Station destination)
        {
            destination.AddedByUser = source.AddedByUser.Name;
            destination.ApprovedByUser = source.ApprovedByUserId == null ? "" : source.ApprovedByUser.Name;
        }
        #endregion
    }
}
