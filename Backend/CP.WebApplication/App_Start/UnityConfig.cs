namespace CP.WebApplication
{
    #region Usings
    using AutoMapper;
    using CP.Common;
    using CP.DataAccess.Factory;
    using CP.Services.Services;
    using CP.Services.Services.Authentication;
    using CP.Services.Services.Direct;
    using System.Web.Http;
    using Unity;
    using Unity.Lifetime;
    using Unity.WebApi;
    #endregion

    public static class UnityConfig
    {
        #region Methods
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            MyConfigureServices(container);
            MyConfigureMapper(container);

            Startup.IoC = container;
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(Startup.IoC);
        }
        #endregion

        #region My Methods
        private static void MyConfigureServices(UnityContainer container)
        {
            container.RegisterType<IAuthenticationService, AuthenticationService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAuthorizationDataProvider, AuthorizationDataProvider>(new HierarchicalLifetimeManager());
            container.RegisterType<IDatabaseContextFactory, DatabaseContextFactory>(new HierarchicalLifetimeManager());
            container.RegisterType<IConfigSettingsProvider, ConfigSettingsProvider>(new HierarchicalLifetimeManager());
            container.RegisterType<IUsersService, UsersService>(new HierarchicalLifetimeManager());
            container.RegisterType<IFuelsService, FuelsService>(new HierarchicalLifetimeManager());
            container.RegisterType<IStationsService, StationsService>(new HierarchicalLifetimeManager());
        }

        private static void MyConfigureMapper(UnityContainer container)
        {
            var config = AutoMapperConfig.CreateConfiguration();
            container.RegisterInstance<IMapper>(new Mapper(config));
        }
        #endregion
    }
}