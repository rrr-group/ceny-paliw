﻿namespace CP.WebApplication
{
    #region Usings
    using CP.Services.Services;
    using CP.Services.Services.Authentication;
    using CP.Services.Services.Direct;
    using CP.WebApplication.Authorization;
    using Microsoft.Owin;
    using Microsoft.Owin.Security.OAuth;
    using Owin;
    using System;
    using Unity;
    #endregion

    public partial class Startup
    {
        #region Properties
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        #endregion

        #region Methods
        public void ConfigureAuth(IAppBuilder app)
        {
            var authenticationService = IoC.Resolve<IAuthenticationService>();
            var usersService = IoC.Resolve<IUsersService>();
            var configSettingsProvider = IoC.Resolve<IConfigSettingsProvider>();

            var simpleAuthorizationServerProvider  = new SimpleAuthorizationServerProvider(authenticationService, usersService);

            var oAuthAuthorizationServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(configSettingsProvider.AccessTokenLifeSpanInMinutes),
                Provider = simpleAuthorizationServerProvider
            };

            OAuthBearerOptions = new OAuthBearerAuthenticationOptions()
            {
                Provider = new CustomOAuthBearerProvider()
            };

            app.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);
            app.UseOAuthAuthorizationServer(oAuthAuthorizationServerOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
        }
        #endregion
    }
}