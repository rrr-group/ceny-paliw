﻿namespace CP.WebApplication.WebApiControllers
{
    #region Usings
    using CP.Common;
    using CP.Services.Model;
    using CP.Services.Services.Direct;
    using CP.WebApplication.Authorization;
    using System.Threading.Tasks;
    using System.Web.Http;
    #endregion

    public class FuelsController : WebApiController
    {
        #region Proporties
        private IFuelsService fuelsService;
        #endregion

        public FuelsController(IAuthorizationDataProvider authorizationDataProvider, IFuelsService fuelsService)
            : base(authorizationDataProvider)
        {
            this.fuelsService = fuelsService;
        }

        #region Methods
        [CustomAuthorization]
        public async Task<IHttpActionResult> PostFuels([FromBody] Fuel fuel)
        {
            this.InitializeAuthorizationData();

            var createdFuel = await this.fuelsService.Add(fuel);
            var resourceLocation = this.GetCreatedResourceLocation(createdFuel.FuelId);

            return this.Created(resourceLocation, createdFuel);
        }

        public async Task<IHttpActionResult> GetFuels(int id)
        {
            var fuels = await this.fuelsService.GetFuelsByStationId(id);
            return this.Ok(fuels);
        }

        [Route("api/Fuels/Names")]
        public async Task<IHttpActionResult> GetFuelNames()
        {
            var fuelNames = await this.fuelsService.GetFuelNames();
            return this.Ok(fuelNames);
        }
        #endregion
    }
}