﻿namespace CP.WebApplication.WebApiControllers
{
    using System;
    using System.Configuration;
    #region Usings
    using System.Web.Http;
    using Common;
    #endregion

    public class WebApiController : ApiController
    {
        #region Properties
        protected IAuthorizationDataProvider AuthorizationDataProvider { get; }
        #endregion

        #region Constructors
        public WebApiController(IAuthorizationDataProvider authorizationDataProvider)
        {
            this.AuthorizationDataProvider = authorizationDataProvider;
        }
        #endregion

        #region Methods
        protected string GetCreatedResourceLocation(int resourceId)
        {
            string requestUri = this.Request.RequestUri.ToString();
            if (!requestUri.EndsWith("/")) requestUri += "/";

            return requestUri + resourceId;
        }

        protected void InitializeAuthorizationData()
        {
            var isAuthorizationEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsAuthorizationEnabled"]);
            this.AuthorizationDataProvider.Initialize(this.Request.Properties, !isAuthorizationEnabled);
        }
        #endregion
    }
}