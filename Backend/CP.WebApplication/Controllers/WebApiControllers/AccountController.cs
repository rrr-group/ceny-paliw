﻿namespace CP.WebApplication.WebApiControllers
{
    #region Usings
    using Common;
    using CP.Services.Model;
    using CP.Services.Services.Authentication;
    using CP.WebApplication.GlobalErrorHandling;
    using Microsoft.AspNet.Identity.Owin;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Http;
    #endregion

    public class AccountController : WebApiController
    {
        #region Proporties
        private IAuthenticationService authenticationService;

        private AppUserManager UserManager
        {
            get
            {
                return Request.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }
        #endregion

        #region Constructors
        public AccountController(IAuthenticationService authenticationService, IAuthorizationDataProvider authorizationDataProvider) 
            : base(authorizationDataProvider)
        {
            this.authenticationService = authenticationService;
        }
        #endregion

        #region Methods
        [AllowAnonymous]
        [ValidateModel]
        public async Task<IHttpActionResult> PostUser(UserRegistrationInfo user)
        {
            var verifyUrl = Url.Link("VerifyEmail", new { id = "{0}", token = "{1}" });
            verifyUrl = HttpUtility.UrlDecode(verifyUrl);
            await this.authenticationService.RegisterUser(this.UserManager, user, verifyUrl);

            return this.Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateModel]
        [Route("api/Account/VerifyEmail", Name = "VerifyEmail")]
        public async Task<IHttpActionResult> VerifyEmail([FromBody] EmailConfirmationInfo email)
        {
            await this.authenticationService.VerifyEmail(this.UserManager, email);

            return this.Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateModel]
        [Route("api/Account/ForgotPassword", Name = "ForgotPassword")]
        public async Task<IHttpActionResult> ForgotPassword([FromBody] ForgotPassword model)
        {
            await this.authenticationService.ForgotPassword(this.UserManager, model.Email);

            return this.Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateModel]
        [Route("api/Account/ResetPassword", Name = "ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword([FromBody] ResetPasswordInfo model)
        {
            await this.authenticationService.ResetPassword(this.UserManager, model);

            return this.Ok();
        }
        #endregion
    }
}