﻿namespace CP.WebApplication.WebApiControllers
{
    #region Usings
    using System.Threading.Tasks;
    using System.Web.Http;
    using CP.Common;
    using CP.Services.Model;
    using CP.Services.Services.Direct;
    using CP.WebApplication.Authorization;
    #endregion

    public class StationsController : WebApiController
    {
        #region Private variables
        private IStationsService stationsService;
        #endregion

        #region Constructors
        public StationsController(IStationsService stationsService, IAuthorizationDataProvider authorizationDataProvider)
            : base(authorizationDataProvider)
        {
            this.stationsService = stationsService;
        }
        #endregion

        #region Methods
        [CustomAuthorization]
        [Route("api/Stations/insert")]
        public async Task<IHttpActionResult> PostStation([FromBody] StationDto station)
        {
            this.InitializeAuthorizationData();

            var createdStation = await this.stationsService.Add(station);
            var resourceLocation = this.GetCreatedResourceLocation(createdStation.StationId);

            return this.Created(resourceLocation, createdStation);
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetFilteredStations([FromBody] StationFilters filters)
        {
            var filteredStations = await this.stationsService.GetFilteredStations(filters);

            return this.Ok(filteredStations);
        }

        public async Task<IHttpActionResult> GetStations()
        {
            var stations = await this.stationsService.GetAllStations();
            return this.Ok(stations);
        }

        [Route("api/Stations/{id}")]
        public async Task<IHttpActionResult> GetStationById(int id)
        {
            var station = await this.stationsService.GetStationById(id);

            return this.Ok(station);
        }
        #endregion
    }
}