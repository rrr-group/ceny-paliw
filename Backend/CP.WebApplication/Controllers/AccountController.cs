﻿using CP.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CP.WebApplication.Controllers
{
    public class AccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLoginModel loginModel)
        {
            if (ModelState.IsValid) 
            {
                return RedirectToAction("Index", "Panel", new { area = "" });
            }

            return View(loginModel);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserRegistrationInfo registerModel)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index", "Panel", new { area = "" });
            }

            return View(registerModel);
        }
    }
}