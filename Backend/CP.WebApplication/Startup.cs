﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(CP.WebApplication.Startup))]
namespace CP.WebApplication
{
    #region Usings
    using Unity;
    #endregion

    public partial class Startup
    {
        public static UnityContainer IoC { get; set; }

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
