﻿namespace CP.WebApplication.Authorization
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using CP.Services;
    using CP.Services.Services.Authentication;
    using CP.Services.Services.Direct;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.OAuth;
    #endregion

    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        #region Private Variables
        private readonly IAuthenticationService authenticationService;
        private readonly IUsersService usersService;
        #endregion

        #region Constructors
        public SimpleAuthorizationServerProvider(IAuthenticationService authenticationService, IUsersService usersService)
        {
            this.authenticationService = authenticationService;
            this.usersService = usersService;
        }
        #endregion

        #region [OAuthAuthorizationServerProvider] methods
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();

            await Task.CompletedTask;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var userManger = context.OwinContext.GetUserManager<AppUserManager>();
            var user = await this.authenticationService.FindUser(userManger, context.UserName, context.Password);
            if (user == null)
            {
                context.SetError("invalid_request", "wrong_credentials");
                return;
            }

            if (!user.EmailConfirmed)
            {
                context.SetError("invalid_request", "email_unverified");
                return;
            }

            //var isUserActive = await this.usersService.IsUserActive(user.Id);
            //if (!isUserActive)
            //{
            //    context.SetError("invalid_request", "subscriber_disabled");
            //    return;
            //}

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", context.UserName));
            using (var dbContext = new AppIdentityContext())
            {
                foreach (var identityUserRole in user.Roles)
                {
                    var role = dbContext.Roles.Find(identityUserRole.RoleId);
                    identity.AddClaim(new Claim(ClaimTypes.Role, role.Name));
                }
            }

            AuthenticationTicket ticket = new AuthenticationTicket(identity, MyCreateProperties(context, user));
            context.Validated(ticket);
        }
        #endregion

        #region My methods
        private static AuthenticationProperties MyCreateProperties(OAuthGrantResourceOwnerCredentialsContext context, AppIdentityUser user)
        {
            var response = new AuthenticationProperties
            {
                ExpiresUtc = DateTime.Now.AddMonths(Constants.OAuthTokenLifetimeInMonths)
            };

            response.Dictionary.Add(new KeyValuePair<string, string>("UserID", user.Id.ToString()));
            return response;
        }
        #endregion
    }
}