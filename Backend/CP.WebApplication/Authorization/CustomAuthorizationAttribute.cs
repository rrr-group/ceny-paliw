﻿namespace CP.WebApplication.Authorization
{
    using CP.Common;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    #region Usings
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    #endregion

    public class CustomAuthorizationAttribute : AuthorizeAttribute
    {
        #region Private constants
        private const string ConstAuthorizationHeaderKey = "Authorization";
        #endregion

        #region Methods
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            return base.IsAuthorized(actionContext);
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var isAuthorizationEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsAuthorizationEnabled"]);
            if (isAuthorizationEnabled == false)
            {
                return;
            }

            var token = MyGetTokenFromRequestHeader(actionContext);

            if(token == string.Empty)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Missing 'Authorization' header. Access denied.");
                return;
            }

            var ticket = Startup.OAuthBearerOptions.AccessTokenFormat.Unprotect(token);
            if(ticket == null)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Invalid token. Access denied");
                return;
            }

            if (!ticket.Properties.Dictionary.TryGetValue(AuthenticationConfiguration.UserIdKey, out string userId))
            {
                userId = string.Empty;
            }

            var roles = ticket.Identity.Claims.Where(claim => claim.Type == ClaimTypes.Role).Select(claim => claim.Value).ToList();

            actionContext.Request.Properties.Add(new KeyValuePair<string, object>(AuthenticationConfiguration.UserIdKey, userId));
            actionContext.Request.Properties.Add(new KeyValuePair<string, object>(AuthenticationConfiguration.UserRolesKey, roles));

            base.OnAuthorization(actionContext);
        }
        #endregion

        #region My methods
        private string MyGetTokenFromRequestHeader(HttpActionContext actionContext)
        {
            return actionContext.Request.Headers.Any(header => header.Key == ConstAuthorizationHeaderKey)
                ? actionContext.Request.Headers.Where(header => header.Key == ConstAuthorizationHeaderKey).FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", string.Empty)
                : string.Empty;
        }
        #endregion
    }
}