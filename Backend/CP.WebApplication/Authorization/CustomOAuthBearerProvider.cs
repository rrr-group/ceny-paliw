﻿namespace CP.WebApplication.Authorization
{
    #region Usings
    using System.Threading.Tasks;
    using Microsoft.Owin.Security.OAuth;
    #endregion

    public class CustomOAuthBearerProvider : OAuthBearerAuthenticationProvider
    {
        #region Public varaibles
        public const string ConstAuthorizationKey = "token";
        #endregion

        #region [OAuthBearerAuthenticationProvider] methods
        public override Task RequestToken(OAuthRequestTokenContext context)
        {
            context.Token = context.Token;
            var tokenFromQuery = context.Request.Query.Get(ConstAuthorizationKey);
            if (!string.IsNullOrEmpty(tokenFromQuery))
            {
                context.Token = tokenFromQuery;
            }

            return base.RequestToken(context);
        }
        #endregion
    }
}