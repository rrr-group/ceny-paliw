$(document).ready(function () {
    $('#registerFormId').validate({
        errorClass: 'invalid-feedback', // You can change the animation class for a different entrance animation - check animations page  
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group > div').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.invalid-feedback').remove();
        },
        success: function (e) {
            e.closest('.form-group').removeClass('has-success has-error');
            e.closest('.invalid-feedback').remove();
        },
        rules: {
            'Login': {
                required: true,
                minlength: 4
            },
            'Email': {
                required: true,
                email: true
            },
            'Password': {
                required: true,
                minlength: 6
            },
            'ConfirmPassword': {
                required: true,
                equalTo: '#Password'
            }
        },
        messages: {
            'Login': {
                required: 'Nazwa uzytkownika jest wymagana',
                minlength: 'Nazwa uzytkownika musi sie skladac z co najmniej 4 znakow'
            },
            'Email': 'Prosze podac prawidlowy adres e-mail',
            'Password': {
                required: 'Prosze podac haslo',
                minlength: 'Twoje haslo musi sie skladac z co najmniej 6 znakow'
            },
            'ConfirmPassword': {
                required: 'Prosze podac haslo',
                minlength: 'Twoje haslo musi sie skladac z co najmniej 6 znakow',
                equalTo: 'Prosze podac to samo haslo co wyzej'
            }
        }
    });

    $('#loginFormId').validate({
        errorClass: 'invalid-feedback', // You can change the animation class for a different entrance animation - check animations page  
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group > div').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.invalid-feedback').remove();
        },
        success: function (e) {
            e.closest('.form-group').removeClass('has-success has-error');
            e.closest('.invalid-feedback').remove();
        },
        rules: {
            'Login': {
                required: true
            },
            'Password': {
                required: true
            }
        },
        messages: {
            'Login': {
                required: 'Nazwa uzytkownika jest wymagana'
            },
            'Password': {
                required: 'Prosze podac haslo'
            }
        }
    });
}); 