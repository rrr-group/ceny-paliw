﻿namespace CP.DataAccess.Model
{
    #region Usings
    using System.Data.Entity;
    using System.Data.Common;
    #endregion

    public partial class Entities : DbContext, IDatabaseContext
    {
        #region Properties
        public bool EnableDispose { get; set; }

        public bool IsDisposed { get; private set; }
        #endregion

        #region Constructors
        public Entities(DbConnection connection)
            : base(connection, true)
        {
            this.IsDisposed = false;
        }
        #endregion

        #region Methods
        protected override void Dispose(bool disposing)
        {
            if (this.EnableDispose)
            {
                base.Dispose(disposing);
                this.IsDisposed = true;
            }
        }
        #endregion
    }
}
