﻿namespace CP.DataAccess.Model
{
    #region Usings
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Threading.Tasks;
    #endregion

    public interface IDatabaseContext : IDisposable
    {
        #region Properties
        bool IsDisposed { get; }

        DbContextConfiguration Configuration { get; }

        Database Database { get; }

        int SaveChanges();

        Task<int> SaveChangesAsync();

        DbSet<AspNetRole> AspNetRoles { get; set; }

        DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }

        DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }

        DbSet<AspNetUser> AspNetUsers { get; set; }

        DbSet<FuelName> FuelNames { get; set; }

        DbSet<Fuel> Fuels { get; set; }

        DbSet<Station> Stations { get; set; }

        DbSet<User> Users { get; set; }
        #endregion
    }
}
