﻿namespace CP.DataAccess.Factory
{
    #region Usings
    using CP.DataAccess.Model;
    #endregion

    public interface IDatabaseContextFactory
    {
        #region Methods
        IDatabaseContext GetDatabaseContext();
        #endregion
    }
}
