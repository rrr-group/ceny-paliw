﻿namespace CP.DataAccess.Factory
{
    #region Usings
    using CP.DataAccess.Model;
    #endregion

    public class DatabaseContextFactory : IDatabaseContextFactory
    {
        public IDatabaseContext GetDatabaseContext()
        {
            return new Entities();
        }
    }
}
