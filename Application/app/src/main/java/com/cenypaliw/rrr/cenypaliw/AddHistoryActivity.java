package com.cenypaliw.rrr.cenypaliw;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.cenypaliw.rrr.cenypaliw.classes.History;
import com.cenypaliw.rrr.cenypaliw.localFilesApi.LocalFiles;
import com.cenypaliw.rrr.cenypaliw.slides.addHistory.addHistorySlide;
import com.cenypaliw.rrr.cenypaliw.slides.addHistory.chooseFuelSlide;

public class AddHistoryActivity extends MaterialIntroActivity {
    public static Integer fuelType, odometer;
    public static Double liters, paid;
    public static String date;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlide(new chooseFuelSlide());
        addSlide(new addHistorySlide());
    }

    @Override
    public void onFinish() {
        addHistory();
        LocalFiles.writeHistory(this);
    }

    private void addHistory() { RefuelingHistoryActivity.history.add(new History(liters, paid, fuelType, date, odometer)); }
}
