package com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes;

import com.google.android.gms.maps.model.LatLng;

public class StationDetails {
    String Name;
    Double Latitude, Longitude;

    public StationDetails(String Name, LatLng position) {
        this.Name = Name;
        Latitude = position.latitude;
        Longitude = position.longitude;
    }
}
