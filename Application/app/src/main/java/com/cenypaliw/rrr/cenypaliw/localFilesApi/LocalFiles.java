package com.cenypaliw.rrr.cenypaliw.localFilesApi;

import android.content.Context;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.RefuelingHistoryActivity;
import com.cenypaliw.rrr.cenypaliw.classes.FuelName;
import com.cenypaliw.rrr.cenypaliw.classes.History;
import com.cenypaliw.rrr.cenypaliw.classes.UserPreferences;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

import static android.content.Context.MODE_APPEND;
import static android.content.Context.MODE_PRIVATE;

public class LocalFiles {
    private static Gson gson = new Gson();
    private static final int FUEL_NAMES = 0;
    private static final int HISTORY = 1;
    private static final int USER_PREFERENCES = 2;
    private static final int FAVOURITE_STATIONS = 3;

    private static void Reading(Context context, int apiNumber) {
        try {
            FileInputStream stream;
            ObjectInputStream din;
            String json;
            switch (apiNumber) {
                case FUEL_NAMES:
                    stream = context.openFileInput("fuelNames.data");
                    din = new ObjectInputStream(stream);
                    json = (String) din.readObject();
                    Type fuelNamesListType = new TypeToken<ArrayList<FuelName>>() {}.getType();
                    MainActivity.fuelNames = gson.fromJson(json, fuelNamesListType);
                    stream.close();
                    break;
                case HISTORY:
                    stream = context.openFileInput("history.data");
                    din = new ObjectInputStream(stream);
                    json = (String) din.readObject();
                    Type historyListType = new TypeToken<ArrayList<History>>() {}.getType();
                    RefuelingHistoryActivity.history = gson.fromJson(json, historyListType);
                    stream.close();
                    break;
                case USER_PREFERENCES:
                    stream = context.openFileInput("userPreferences.data");
                    din = new ObjectInputStream(stream);
                    json = (String) din.readObject();
                    MainActivity.userPreferences = gson.fromJson(json, UserPreferences.class);
                    stream.close();
                    break;
                case FAVOURITE_STATIONS:
                    stream = context.openFileInput("favoriteStations.data");
                    din = new ObjectInputStream(stream);
                    json = (String) din.readObject();
                    Type favoriteStationsListType = new TypeToken<ArrayList<Integer>>(){}.getType();
                    MainActivity.favoriteStations = gson.fromJson(json, favoriteStationsListType);
                    stream.close();
                    break;
                default:
                    break;
            }
        } catch(IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void readFuelNames(Context context) { if (MainActivity.fuelNames == null) Reading(context, FUEL_NAMES); }
    public static void readHistory(Context context) { Reading(context, HISTORY); }
    public static void readUserPreferences(Context context) { Reading(context, USER_PREFERENCES); }
    public static void readFavoriteStations(Context context) { Reading(context, FAVOURITE_STATIONS); }

    private static void Writing(Context context, int apiNumber) {
        try {
            FileOutputStream stream;
            ObjectOutputStream dout;
            switch (apiNumber) {
                default:
                case FUEL_NAMES:
                    stream = context.openFileOutput("fuelNames.data", MODE_PRIVATE);
                    dout = new ObjectOutputStream(stream);
                    dout.writeObject(gson.toJson(MainActivity.fuelNames));
                    break;
                case HISTORY:
                    stream = context.openFileOutput("history.data", MODE_PRIVATE);
                    dout = new ObjectOutputStream(stream);
                    dout.writeObject(gson.toJson(RefuelingHistoryActivity.history));
                    break;
                case USER_PREFERENCES:
                    stream = context.openFileOutput("userPreferences.data", MODE_PRIVATE);
                    dout = new ObjectOutputStream(stream);
                    dout.writeObject(gson.toJson(MainActivity.userPreferences));
                    break;
                case FAVOURITE_STATIONS:
                    Collections.sort(MainActivity.favoriteStations);
                    stream = context.openFileOutput("favoriteStations.data", MODE_PRIVATE);
                    dout = new ObjectOutputStream(stream);
                    dout.writeObject(gson.toJson(MainActivity.favoriteStations));
            }
            dout.flush();
            stream.getFD().sync();
            stream.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    public static void writeFuelNames(Context context) { Writing(context, FUEL_NAMES); }
    public static void writeHistory(Context context) { Writing(context, HISTORY); }
    public static void writeUserPreferences(Context context) { Writing(context, USER_PREFERENCES); }
    public static void writeFavoriteStations(Context context) { Writing(context, FAVOURITE_STATIONS); }

    public static void makingFilesIfNotExisting(Context context) {
        try {
            String[] files = { "fuelNames.data", "history.data", "userPreferences.data", "favorite.data" };
            for (String file : files) {
                FileOutputStream fileOutputStream = context.openFileOutput(file, MODE_APPEND);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                outputStreamWriter.write("");
                outputStreamWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}