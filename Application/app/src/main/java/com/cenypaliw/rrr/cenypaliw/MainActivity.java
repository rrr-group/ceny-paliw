package com.cenypaliw.rrr.cenypaliw;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.cenypaliw.rrr.cenypaliw.classes.*;
import com.cenypaliw.rrr.cenypaliw.localFilesApi.LocalFiles;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.Connection;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends FragmentActivity implements GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener, OnMapReadyCallback {

    private static GoogleMap mMap;
    private static ArrayList<Marker> markers = new ArrayList<>();
    private static View infoWindow;
    private static boolean stopTrackingLocation = false;

    private LocationManager lm;

    static int clickedMarkerID = -1;
    static boolean isLoginSkipped = false;

    public static LatLng myPosition;
    public static Marker marker;
    public static UserPreferences userPreferences = new UserPreferences();
    public static Token token;
    public static Context context;
    public static ArrayList<Station> stations;
    public static ArrayList<FuelName> fuelNames;
    public static ArrayList<Integer> favoriteStations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        LocalFiles.makingFilesIfNotExisting(this);
        LocalFiles.readUserPreferences(this);
        if (!userPreferences.showedIntroduction)
            startActivity(new Intent(this, IntroductionActivity.class));
        if (userPreferences.fuelType == -1 || userPreferences.radius == 0 || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            startActivity(new Intent(this, UserPreferencesActivity.class));
        if (!isLoginSkipped && token == null)
            startActivity(new Intent(this, LoginActivity.class));
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        Objects.requireNonNull(mapFragment).getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setCustomInfoAdapter();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            setLocationListener();
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnCameraMoveStartedListener(reason -> {
            if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE || reason == GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION)
                stopTrackingLocation = true;
        });
        mMap.setOnMarkerClickListener(marker -> {
            clickedMarkerID = (int)(marker.getTag());
            MainActivity.marker = marker;
            return false;
        });
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        if (!markers.isEmpty())
        {
            markers.clear();
            for (Station station : stations) {
                setMarker(station);
            }
            mMap.animateCamera(CameraUpdateFactory.zoomTo( 10.0f ));
        }
    }

    private void setCustomInfoAdapter() {
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public View getInfoWindow(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.map_info_window, null);
                TextView title = v.findViewById(R.id.title_window);
                TextView snippet = v.findViewById(R.id.snippet_window);
                stopTrackingLocation = true;
                title.setText(marker.getTitle());
                snippet.setText(marker.getSnippet());
                MainActivity.marker = marker;
                MainActivity.infoWindow = v;
                FloatingActionButton addPriceButton = v.findViewById(R.id.addPriceButton);
                FloatingActionButton addToFavoritesButton = v.findViewById(R.id.addToFavoritesButton);
                FloatingActionButton informationAboutStationButton = v.findViewById(R.id.informationsAboutStationButton);
                int stationId = stations.get(clickedMarkerID).stationId;
                if (favoriteStations != null && favoriteStations.contains(stationId))
                    addToFavoritesButton.setImageResource(R.drawable.favorite_clicked_icon);
                else
                    addToFavoritesButton.setImageResource(R.drawable.favorite_icon);
                addPriceButton.show();
                if (stations.get((int)marker.getTag()).fuels.isEmpty() || token == null)
                    addPriceButton.setAlpha(0.5f);
                else {
                    addPriceButton.setOnTouchListener(new OnInfoWindowElemTouchListener(addPriceButton) {
                        @Override
                        protected void onClickConfirmed(View v, Marker marker) {
                            if (clickedMarkerID > -1) {
                                startActivity(new Intent(context, AddFuelPriceActivity.class));
                            } else {
                                Toast.makeText(context, "Proszę wybrać stację", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                addToFavoritesButton.setOnTouchListener(new OnInfoWindowElemTouchListener(addPriceButton){
                    @Override
                    protected void onClickConfirmed(View v, Marker marker) {
                        changeStateOfFavoriteStations();
                        marker.showInfoWindow();
                    }
                });
                informationAboutStationButton.setOnTouchListener(new OnInfoWindowElemTouchListener(addPriceButton){
                    @Override
                    protected void onClickConfirmed(View v, Marker marker) { startActivity(new Intent(context, InformationAboutStationActivity.class)); }
                });
                return v;
            }
            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });
        mMap.setOnInfoWindowCloseListener(v -> stopTrackingLocation = false);
    }

    private void changeStateOfFavoriteStations() {
        if(clickedMarkerID > -1) {
            int stationId = stations.get(clickedMarkerID).stationId;
            if (favoriteStations != null && favoriteStations.contains(stationId)) {
                favoriteStations.remove((Integer) stationId);
                markers.get(clickedMarkerID).setIcon((BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            }
            else {
                if (favoriteStations == null) {
                    favoriteStations = new ArrayList<>();
                    favoriteStations.add(stationId);
                }
                else {
                    favoriteStations.add(stationId);
                }
                markers.get(clickedMarkerID).setIcon((BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
            }
            LocalFiles.writeFavoriteStations(this);
        }
        else {
            Toast.makeText(this, "Proszę wybrać stację", Toast.LENGTH_SHORT).show();
        }
    }

    public static void setMarker(Station station)
    {
        MarkerOptions newMarker = new MarkerOptions();
        newMarker.position(station.getPosition())
                .visible(true)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        String txt = getIndexOfFuelFromStationByFuelNameId(station);
        newMarker.title(station.name);
        if (favoriteStations != null && favoriteStations.contains(station.stationId))
            newMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
        if (txt != null)
            newMarker.snippet(txt);
        else if (!station.isApproved || !station.isActive)
            newMarker.alpha(0.1f).snippet("Stacja nie została jeszcze zatwierdzona");
        else if (station.fuels.isEmpty())
            newMarker.alpha(0.3f).snippet("Do stacji nie dodano jeszcze paliw");
        else
            newMarker.alpha(0.75f).snippet("Brak wybranego paliwa.");
        Marker addedMarker = mMap.addMarker(newMarker);
        addedMarker.setTag(getStationNumberByStationId(station.stationId));
        markers.add(addedMarker);
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Obecna pozycja:\n" + location.getLongitude() + " " + location.getLatitude(), Toast.LENGTH_SHORT).show();
    }

    private static String getIndexOfFuelFromStationByFuelNameId(Station station)
    {
        for (int i = 0; i < station.fuels.size(); i++) {
            if (station.fuels.get(i).fuelNameId == userPreferences.fuelType) {
                float price = ((float)(((int)(station.fuels.get(i).price * 100)))) / 100;
                return price + " zł - " + getFuelNameByFuelNameId(userPreferences.fuelType);
            }
        }
        return null;
    }

    static String getFuelNameByFuelNameId(int fuelNameId)
    {
        for (FuelName fuelName : fuelNames) {
            if (fuelName.fuelNameId == fuelNameId) {
                return fuelName.name;
            }
        }
        return "Fuel name not found";
    }

    private static int getStationNumberByStationId(int stationId)
    {
        for(int i = 0; i < stations.size(); i++) {
            if (stations.get(i).stationId == stationId) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        stopTrackingLocation = false;
        getLocation();
        return false;
    }

    @SuppressLint("MissingPermission")
    private void getLocation() {
        if (!stopTrackingLocation) {
            Location l = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (l != null) {
                myPosition = new LatLng(l.getLatitude(), l.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(myPosition));
                if (myPosition.longitude != 0 && myPosition.latitude != 0 && userPreferences.radius != 0 && stations == null) {
                    Connection.getResources(myPosition, userPreferences.radius, this);
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(10.0f));
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void setLocationListener() {
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                getLocation();
            }
            public void onStatusChanged(String provider, int status, Bundle extras) {}
            public void onProviderEnabled(String provider) {}
            public void onProviderDisabled(String provider) {}
        };
        lm.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, locationListener);
    }

    public void showMenu(View v) {
        v.setX(-150);
        v.setY(-170);
        PopupMenu menu = new PopupMenu(this, v);
        menu.getMenuInflater().inflate(R.menu.main_menu, menu.getMenu());
        if (token == null)
            menu.getMenu().add(1, 1, 1,"Zaloguj się");
        else
            menu.getMenu().add(1, 0, 0,"Dodaj stację");
        menu.setOnMenuItemClickListener(item -> {
            switch(item.getItemId())
            {
                case R.id.refuelingHistoryItem:
                    refuelingHistory();
                    break;
                case R.id.calculatorItem:
                    fuelConsumptionCalculator();
                    break;
                case R.id.showIntroductionItem:
                    userPreferences.showedIntroduction = false;
                    LocalFiles.writeUserPreferences(this);
                    recreate();
                    break;
                case 0:
                    if (myPosition != null)
                        startActivity(new Intent(this, AddStationActivity.class));
                    else
                        Toast.makeText(this, "Spróbuj ponownie, gdy znajdzie twoją lokalizację", Toast.LENGTH_LONG).show();
                    break;
                case 1:
                    startActivity(new Intent(this, LoginActivity.class));
                    break;
            }
            return true;
        });
        menu.setOnDismissListener((event) -> {
            v.setX(50);
            v.setY(50);
        });
        menu.show();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean ret = false;
        if (marker != null && marker.isInfoWindowShown() && mMap != null && infoWindow != null) {
            Point point = mMap.getProjection().toScreenLocation(marker.getPosition());
            MotionEvent copyEv = MotionEvent.obtain(ev);
            copyEv.offsetLocation(
                    -point.x + (infoWindow.getWidth() / 2),
                    -point.y + infoWindow.getHeight() + 10);
            ret = infoWindow.dispatchTouchEvent(copyEv);
        }
        return ret || super.dispatchTouchEvent(ev);
    }

    public void refreshFilters(View view) {
        stations = null;
        markers.clear();
        userPreferences.setDefault();
        stopTrackingLocation = false;
        LocalFiles.writeUserPreferences(this);
        recreate();
    }

    private void fuelConsumptionCalculator() { startActivity(new Intent(this, FuelConsumptionCalculatorActivity.class)); }

    private void refuelingHistory() { startActivity(new Intent(this, RefuelingHistoryActivity.class)); }

}
