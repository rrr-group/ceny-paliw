package com.cenypaliw.rrr.cenypaliw;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.cenypaliw.rrr.cenypaliw.classes.Station;

import java.util.ArrayList;
import java.util.List;

public class InformationAboutStationActivity extends AppCompatActivity {
    private static Station station;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_about_station);
        station = MainActivity.stations.get(MainActivity.clickedMarkerID);
        TextView stationTitle = findViewById(R.id.stationTitle);
        TextView fuelPricesTitle = findViewById(R.id.fuelPricesTitle);
        ListView fuelPricesList = findViewById(R.id.fuelPricesList);
        FloatingActionButton informationAddButton = findViewById(R.id.informationAddButton);
        stationTitle.setText(station.name);
        showStationDescription();
        if (station.fuels.isEmpty()) {
            fuelPricesTitle.setVisibility(View.GONE);
            fuelPricesList.setVisibility(View.GONE);
            informationAddButton.setVisibility(View.GONE);
        }
        else {
            if (MainActivity.token == null)
                informationAddButton.setVisibility(View.GONE);
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getListOfFuels());
            fuelPricesList.setAdapter(arrayAdapter);
        }
    }

    private List<String> getListOfFuels()
    {
        ArrayList<String> fuelsList = new ArrayList<>();
        for(int i = 0; i < station.fuels.size(); i++) {
            fuelsList.add(MainActivity.getFuelNameByFuelNameId(station.fuels.get(i).fuelNameId) + ": " + station.fuels.get(i).price + " zł");
        }
        return fuelsList;
    }

    private void showStationDescription() {
        TextView stationDescription = findViewById(R.id.stationDescription);
        stationDescription.setText(station.description.replace(';', '\n')); //TODO: text with icons
    }

    public void addPrice(View view) { startActivity(new Intent(this, AddFuelPriceActivity.class)); }
    public void onBackPressed(View view) { finish(); }
}
