package com.cenypaliw.rrr.cenypaliw.classes;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.google.android.gms.maps.model.Marker;

public abstract class OnInfoWindowElemTouchListener implements OnTouchListener {
    private final View view;
    private Marker marker;

    public OnInfoWindowElemTouchListener(View view) {
        this.view = view;
    }

    @Override
    public boolean onTouch(View vv, MotionEvent event) {
        this.marker = MainActivity.marker;
        if (0 <= event.getX() && event.getX() <= view.getWidth() && 0 <= event.getY() && event.getY() <= view.getHeight())
        {
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN: onClickConfirmed(view, marker); break;
                default: break;
            }
        }
        return false;
    }

    protected abstract void onClickConfirmed(View v, Marker marker);
}