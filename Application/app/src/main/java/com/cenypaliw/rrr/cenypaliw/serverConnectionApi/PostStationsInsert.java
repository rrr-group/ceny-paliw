package com.cenypaliw.rrr.cenypaliw.serverConnectionApi;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.classes.Station;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes.StationDetails;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes.StationsFilterDetails;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class PostStationsInsert extends AsyncTask<Void, Void, Station> {
    private final String TAG = "PostStationsInsert";
    private final String wholeURL = Connection.siteURL + "api/Stations/insert";

    StationDetails newStation;

    @Override
    protected Station doInBackground(Void... params) {
        try {
            URL siteEndpoint = new URL(wholeURL);
            HttpURLConnection myConnection = (HttpURLConnection) siteEndpoint.openConnection();
            return serverRequest(myConnection);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Station serverRequest(HttpURLConnection myConnection) throws IOException {
        Gson gson = new Gson();
        myConnection.setRequestProperty("Content-Type", "application/json");
        myConnection.setRequestProperty("Authorization", "Bearer " + MainActivity.token.access_token);
        myConnection.setRequestMethod("POST");
        myConnection.setDoOutput(true);
        OutputStream outputStream = myConnection.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStreamWriter.write(gson.toJson(newStation));
        outputStreamWriter.flush();
        outputStreamWriter.close();
        outputStream.close();
        int responseCode = myConnection.getResponseCode();
        if (responseCode == 200 || responseCode == 201) {
            return serverResponse(myConnection);
        } else {
            Log.w(TAG, "Problem połączenia z serwerem: " + responseCode);
            if (responseCode == 400) {
                Connection.serverErrorResponse(myConnection, TAG);
            }
        }
        return null;
    }

    private Station serverResponse(HttpURLConnection myConnection) throws IOException {
        InputStream responseBody = myConnection.getInputStream();
        Reader reader = new InputStreamReader(responseBody, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        Station station = gson.fromJson(reader, Station.class);
        return station;
    }

    @Override
    protected void onPostExecute(Station station) {
        super.onPostExecute(station);
        MainActivity.stations.add(station);
        Connection.getFuelsStationId(station);
        Toast.makeText(MainActivity.context, "Dodano stację", Toast.LENGTH_LONG).show();
    }
}
