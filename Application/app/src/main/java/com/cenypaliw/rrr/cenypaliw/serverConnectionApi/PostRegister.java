package com.cenypaliw.rrr.cenypaliw.serverConnectionApi;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.cenypaliw.rrr.cenypaliw.RegisterActivity;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes.AccountDetails;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes.ErrorResponse;
import com.google.gson.Gson;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class PostRegister extends AsyncTask<Void, Void, Void> {
    private final String TAG = "PostRegister";
    private final String wholeURL = Connection.siteURL + "api/Account";

    Context context;
    AccountDetails account;

    private ErrorResponse errorResponse;

    @Override
    protected Void doInBackground(Void... params) {
        try {
            URL siteEndpoint = new URL(wholeURL);
            HttpURLConnection myConnection = (HttpURLConnection) siteEndpoint.openConnection();
            return serverRequest(myConnection);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Void serverRequest(HttpURLConnection myConnection) throws IOException {
        Gson gson = new Gson();
        myConnection.setRequestProperty("Content-Type", "application/json");
        myConnection.setRequestMethod("POST");
        myConnection.setDoOutput(true);
        OutputStream outputStream = myConnection.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStreamWriter.write(gson.toJson(account));
        outputStreamWriter.flush();
        outputStreamWriter.close();
        outputStream.close();
        int responseCode = myConnection.getResponseCode();
        if (responseCode == 200) {
            return null;
        } else {
            Log.w(TAG, "Problem połączenia z serwerem: " + responseCode);
            if (responseCode == 400)
                serverErrorResponse(myConnection);
        }
        return null;
    }

    private void serverErrorResponse(HttpURLConnection myConnection) {
        RegisterActivity.isError = true;
        InputStream responseBody = myConnection.getErrorStream();
        Reader reader = new InputStreamReader(responseBody, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        errorResponse = gson.fromJson(reader, ErrorResponse.class);
    }

    @Override
    protected void onPostExecute(Void param) {
        super.onPostExecute(null);
        if (RegisterActivity.isError) {
            if (errorResponse.Message != null && errorResponse.Message.length() > 10)
                Toast.makeText(context, errorResponse.Message, Toast.LENGTH_LONG).show();
            else if (errorResponse.Code == 1002)
                Toast.makeText(context, "Email zajęty", Toast.LENGTH_LONG).show();
            else if (errorResponse.Code == 1004)
                Toast.makeText(context, "Nazwa użytkownika zajęta", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(context, errorResponse.Code + ": nieznany błąd", Toast.LENGTH_LONG).show();
        }
        RegisterActivity.mProgressView.animate().cancel();
    }
}
