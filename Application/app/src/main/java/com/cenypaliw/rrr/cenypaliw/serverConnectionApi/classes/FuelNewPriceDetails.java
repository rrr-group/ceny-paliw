package com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes;

public class FuelNewPriceDetails {
    int FuelNameId;
    int StationId;
    double Price;

    public FuelNewPriceDetails(int fuelNameId, int stationId, double price) {
        FuelNameId = fuelNameId;
        StationId = stationId;
        Price = price;
    }
}
