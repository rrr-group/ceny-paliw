package com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes;

import com.google.android.gms.maps.model.LatLng;

public class StationsFilterDetails {
    private double Longitude;
    private double Latitude;
    private int Radius;

    public StationsFilterDetails(LatLng myPosition, int radius) {
        Longitude = myPosition.longitude;
        Latitude = myPosition.latitude;
        Radius = radius;
    }
}
