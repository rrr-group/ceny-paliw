package com.cenypaliw.rrr.cenypaliw.serverConnectionApi;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.classes.Station;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes.StationsFilterDetails;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class PostStations extends AsyncTask<Void, Void, ArrayList<Station>> {
    private final String TAG = "PostStations";
    private final String wholeURL = Connection.siteURL + "api/Stations";

    LatLng myPosition;
    int radius;

    @Override
    protected ArrayList<Station> doInBackground(Void... params) {
        try {
            URL siteEndpoint = new URL(wholeURL);
            HttpURLConnection myConnection = (HttpURLConnection) siteEndpoint.openConnection();
            return serverRequest(myConnection);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Station> serverRequest(HttpURLConnection myConnection) throws IOException {
        Gson gson = new Gson();
        myConnection.setRequestProperty("Content-Type", "application/json");
        myConnection.setRequestMethod("POST");
        myConnection.setDoOutput(true);
        OutputStream outputStream = myConnection.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStreamWriter.write(gson.toJson(new StationsFilterDetails(myPosition, radius)));
        outputStreamWriter.flush();
        outputStreamWriter.close();
        outputStream.close();
        int responseCode = myConnection.getResponseCode();
        if (responseCode == 200 || responseCode == 201) {
            return serverResponse(myConnection);
        } else {
            Log.w(TAG, "Problem połączenia z serwerem: " + responseCode);
            if (responseCode == 400) {
                Connection.serverErrorResponse(myConnection, TAG);
            }
        }
        return null;
    }

    private ArrayList<Station> serverResponse(HttpURLConnection myConnection) throws IOException {
        InputStream responseBody = myConnection.getInputStream();
        Reader reader = new InputStreamReader(responseBody, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        Type stationsListType = new TypeToken<ArrayList<Station>>(){}.getType();
        List<Station> stationsList = gson.fromJson(reader, stationsListType);
        return (ArrayList<Station>) stationsList;
    }

    @Override
    protected void onPostExecute(ArrayList<Station> stations) {
        super.onPostExecute(stations);
        if (stations != null) {
            if (MainActivity.favoriteStations != null) {
                ArrayList<Integer> favoriteStations = (ArrayList<Integer>) MainActivity.favoriteStations.clone();
                for (Station station : stations) {
                    if (favoriteStations.contains(station.stationId)) {
                        favoriteStations.remove((Object) station.stationId);
                    }
                    station.fuels = new ArrayList<>();
                    Connection.getFuelsStationId(station);
                }
                for (int stationId : favoriteStations) {
                    Connection.getStationStationId(stationId);
                }
            } else {
                for (Station station : stations) {
                    station.fuels = new ArrayList<>();
                    Connection.getFuelsStationId(station);
                }
            }
            MainActivity.stations = stations;
        }
        else {
            Toast.makeText(MainActivity.context, "Problem połączenia z serwerem", Toast.LENGTH_LONG).show();
        }
    }
}
