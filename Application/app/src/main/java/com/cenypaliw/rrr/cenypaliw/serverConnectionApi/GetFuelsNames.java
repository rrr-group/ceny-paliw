package com.cenypaliw.rrr.cenypaliw.serverConnectionApi;

import android.os.AsyncTask;
import android.util.Log;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.classes.FuelName;
import com.cenypaliw.rrr.cenypaliw.localFilesApi.LocalFiles;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class GetFuelsNames extends AsyncTask<Void, Void, ArrayList<FuelName>>{
    private final String TAG = "GetFuelsNames";
    private final String wholeURL = Connection.siteURL + "api/Fuels/Names";

    @Override
    protected ArrayList<FuelName> doInBackground(Void... params) {
        try {
            URL siteEndpoint = new URL(wholeURL);
            HttpURLConnection myConnection = (HttpURLConnection) siteEndpoint.openConnection();
            return serverRequest(myConnection);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<FuelName> serverRequest(HttpURLConnection myConnection) throws IOException {
        myConnection.setRequestProperty("Content-Type", "application/json");
        myConnection.setRequestMethod("GET");
        int responseCode = myConnection.getResponseCode();
        if (responseCode == 200 || responseCode == 201) {
            return serverResponse(myConnection);
        } else {

            Log.w(TAG,"Problem połączenia z serwerem: " + responseCode);
            if (responseCode == 400) {
                Connection.serverErrorResponse(myConnection, TAG);
            }
        }
        return null;
    }

    private ArrayList<FuelName> serverResponse(HttpURLConnection myConnection) throws IOException {
        InputStream responseBody = myConnection.getInputStream();
        Reader reader = new InputStreamReader(responseBody, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        Type FuelNamelistType = new TypeToken<ArrayList<FuelName>>(){}.getType();
        List<FuelName> fuelNameList = gson.fromJson(reader, FuelNamelistType);
        return (ArrayList<FuelName>) fuelNameList;
    }

    @Override
    protected void onPostExecute(ArrayList<FuelName> fuelNames) {
        super.onPostExecute(fuelNames);
        MainActivity.fuelNames = fuelNames;
        LocalFiles.writeFuelNames(MainActivity.context);
    }
}
