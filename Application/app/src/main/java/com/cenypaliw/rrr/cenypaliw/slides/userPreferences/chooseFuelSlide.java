package com.cenypaliw.rrr.cenypaliw.slides.userPreferences;

import agency.tango.materialintroscreen.SlideFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.R;

import java.util.ArrayList;
import java.util.List;

public class chooseFuelSlide extends SlideFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_choose_fuel_slide, container, false);
        ListView listView = view.findViewById(R.id.fuelNamesListView);
        TextView fuelTypeTextView = view.findViewById(R.id.text);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_list_item_1, getListOfFuelNames());
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener((parent, v, position, id) -> {
            MainActivity.userPreferences.fuelType = position + 1;
            fuelTypeTextView.setText("Wybrano:\n" + MainActivity.fuelNames.get(position).name);
        });
        return view;
    }

    @Override
    public int backgroundColor() {
        return R.color.colorPrimary;
    }

    @Override
    public int buttonsColor() {
        return R.color.colorInputs;
    }

    @Override
    public boolean canMoveFurther() {
        return MainActivity.userPreferences.fuelType > -1;
    }

    @Override
    public String cantMoveFurtherErrorMessage() {
        return "Nie wybrano nazwy paliwa";
    }

    private List<String> getListOfFuelNames()
    {
        ArrayList<String> fuelNamesList = new ArrayList<>();
        for(int i = 0; i < MainActivity.fuelNames.size(); i++) {
            fuelNamesList.add(MainActivity.fuelNames.get(i).name);
        }
        return fuelNamesList;
    }
}
