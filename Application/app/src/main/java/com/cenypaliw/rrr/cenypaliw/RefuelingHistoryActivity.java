package com.cenypaliw.rrr.cenypaliw;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.cenypaliw.rrr.cenypaliw.classes.History;
import com.cenypaliw.rrr.cenypaliw.localFilesApi.LocalFiles;

import java.util.ArrayList;
import java.util.List;

public class RefuelingHistoryActivity extends AppCompatActivity {
    public static ArrayList<History> history = new ArrayList<>();

    public Context context;

    private TextView text;
    private ListView listView;
    private FloatingActionButton removeButton;
    private int position = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refueling_history);
        context = this;
        text = findViewById(R.id.historyText);
        listView = findViewById(R.id.historyListView);
        removeButton = findViewById(R.id.historyDeleteButton);
        LocalFiles.readFuelNames(context);
        LocalFiles.readHistory(this);
        viewUpdate();
    }

    private void viewUpdate()
    {
        if (position == -1)
            removeButton.setAlpha(0.5f);
        if (history == null || history.isEmpty()) {
            text.setVisibility(View.VISIBLE);
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, new ArrayList<>());
            listView.setAdapter(arrayAdapter);
        }
        else {
            text.setVisibility(View.INVISIBLE);
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, getHistoryList());
            listView.setAdapter(arrayAdapter);
            listView.setOnItemClickListener((parent, view, position, id) -> {
                this.position = position;
                removeButton.setAlpha(1.0f);
                for (int i = listView.getFirstVisiblePosition(); i <= listView.getLastVisiblePosition(); i++) {
                    if (i == position)
                        listView.getChildAt(i).setBackgroundColor(ContextCompat.getColor(this, R.color.colorInputs));
                    else
                        listView.getChildAt(i).setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                }
            });
        }
    }

    @SuppressLint("DefaultLocale")
    private static List<String> getHistoryList()
    {
        int odometerValue = 0;
        ArrayList<String> fuelsList = new ArrayList<>();
        for (History obj : history) {
             String row = getFuelName(obj.fuelNameId) + "\n" +
                    String.format("%.02f", obj.paid) + "zł / " + String.format("%.02f", obj.liters) + "l = " + String.format("%.02f", (obj.paid / obj.liters)) + "zł/l\n";
            if (odometerValue > 0 && obj.liters > 0 && ((double)(obj.odometer - odometerValue) / 100) > 0) {
                row += String.format("%.02f", (obj.liters / ((double)(obj.odometer - odometerValue) / 100))) + "l/100km\n";
            }
            row += "Wskazanie licznika: " + obj.odometer + "km\n" +
                    "Tankowano dnia " + obj.date;
            odometerValue = obj.odometer;
            fuelsList.add(row);
        }
        return fuelsList;
    }

    public void removeItem(View v) {
        if (position > -1) {
            history.remove(position);
            LocalFiles.writeHistory(context);
            position = -1;
            viewUpdate();
        }
    }

    public void onResume() {
        super.onResume();
        viewUpdate();
    }

    public void onBackPressed(View view) { finish(); }
    public void addHistory(View view) { startActivity(new Intent(this, AddHistoryActivity.class)); }
    private static String getFuelName(int fuelNameId) { return MainActivity.getFuelNameByFuelNameId(fuelNameId); }
}
