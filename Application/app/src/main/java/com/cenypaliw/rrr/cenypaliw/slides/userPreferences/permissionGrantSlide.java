package com.cenypaliw.rrr.cenypaliw.slides.userPreferences;

import agency.tango.materialintroscreen.SlideFragment;
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.cenypaliw.rrr.cenypaliw.R;
import pub.devrel.easypermissions.EasyPermissions;

public class permissionGrantSlide extends SlideFragment {
    private final int REQUEST_LOCATION_PERMISSION = 1;
    private Button grantPermissions;
    private TextView title, description;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_permission_grant_slide, container, false);
        grantPermissions = view.findViewById(R.id.permission_grant_button);
        title = view.findViewById(R.id.txt_title_slide);
        description = view.findViewById(R.id.txt_description_slide);
        grantPermissions.setOnClickListener((v)->EasyPermissions.requestPermissions(this, "Uprawnienia są potrzebne do wyznaczenia twojej pozycji.", REQUEST_LOCATION_PERMISSION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION));
        return view;
    }

    @Override
    public boolean canMoveFurther() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            grantPermissions.setVisibility(View.GONE);
            title.setText("Uprawnienia przyznane");
            description.setText("Proszę przejść do następnej strony w celu wybrania paliwa, którego chcemy zobaczyć ceny");
            android.os.Process.killProcess(android.os.Process.myPid());
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        canMoveFurther();
    }

    @Override
    public int backgroundColor() { return R.color.colorPrimary; }

    @Override
    public int buttonsColor() { return R.color.colorInputs; }

    @Override
    public String cantMoveFurtherErrorMessage() { return "Musisz nadać uprawnienia do lokaliazacji"; }
}
