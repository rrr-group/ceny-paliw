package com.cenypaliw.rrr.cenypaliw.serverConnectionApi;

import android.os.AsyncTask;
import android.util.Log;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.classes.Fuel;
import com.cenypaliw.rrr.cenypaliw.classes.Station;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class GetFuelsStationId extends AsyncTask<Void, Void, ArrayList<Fuel>> {
    private final String TAG = "GetFuelsStationId";

    private String wholeURL;

    public Station station;

    @Override
    protected ArrayList<Fuel> doInBackground(Void... params) {
        try {
            wholeURL = Connection.siteURL + "api/Fuels/" + station.stationId;
            URL siteEndpoint = new URL(wholeURL);
            HttpURLConnection myConnection = (HttpURLConnection) siteEndpoint.openConnection();
            return serverRequest(myConnection);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Fuel> serverRequest(HttpURLConnection myConnection) throws IOException {
        myConnection.setRequestProperty("Content-Type", "application/json");
        myConnection.setRequestMethod("GET");
        int responseCode = myConnection.getResponseCode();
        if (responseCode == 200 || responseCode == 201) {
            return serverResponse(myConnection);
        } else {
            Log.w(TAG, "Problem połączenia z serwerem: " + responseCode);
            if (responseCode == 400) {
                Connection.serverErrorResponse(myConnection, TAG);
            }
        }
        return null;
    }

    private ArrayList<Fuel> serverResponse(HttpURLConnection myConnection) throws IOException {
        InputStream responseBody = myConnection.getInputStream();
        Reader reader = new InputStreamReader(responseBody, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        Type fuelsListType = new TypeToken<ArrayList<Fuel>>(){}.getType();
        List<Fuel> fuelsList = gson.fromJson(reader, fuelsListType);
        return (ArrayList<Fuel>) fuelsList;
    }

    @Override
    protected void onPostExecute(ArrayList<Fuel> fuels) {
        super.onPostExecute(fuels);
        station.fuels = fuels;
        MainActivity.setMarker(station);
    }
}
