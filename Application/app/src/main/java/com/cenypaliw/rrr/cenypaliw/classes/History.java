package com.cenypaliw.rrr.cenypaliw.classes;

public class History {
    public double liters, paid;
    public int fuelNameId, odometer;
    public String date;

    public History(double liters, double paid, int fuelNameId, String date, int odometer) {
        this.liters = liters;
        this.paid = paid;
        this.fuelNameId = fuelNameId;
        this.date = date;
        this.odometer = odometer;
    }
}
