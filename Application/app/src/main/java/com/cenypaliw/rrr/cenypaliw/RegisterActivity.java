package com.cenypaliw.rrr.cenypaliw;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.cenypaliw.rrr.cenypaliw.localFilesApi.LocalFiles;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.Connection;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes.AccountDetails;

public class RegisterActivity extends AppCompatActivity {
    public static View mProgressView;
    public static View mRegisterFormView;
    public static boolean isError;

    private static Context context;

    private EditText loginView;
    private EditText mPasswordView;
    private EditText mRepeatPasswordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        this.context = this;
        loginView = findViewById(R.id.login);
        mPasswordView = findViewById(R.id.password);
        mRepeatPasswordView = findViewById(R.id.repeatPassword);
        findViewById(R.id.register_button).setOnClickListener(view -> attemptRegister());
        mRegisterFormView = findViewById(R.id.register_form);
        mProgressView = findViewById(R.id.register_progress);
    }

    private void attemptRegister() {
        loginView.setError(null);
        mPasswordView.setError(null);
        mRepeatPasswordView.setError(null);

        String login = loginView.getText().toString();
        String password = mPasswordView.getText().toString();
        String repeatedPassword = mRepeatPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError("Niepoprawne hasło");
            focusView = mPasswordView;
            cancel = true;
        }
        if (!TextUtils.equals(password, repeatedPassword)) {
            mRepeatPasswordView.setError("Hasła muszą być takie same");
            focusView = mPasswordView;
            cancel = true;
        }
        if (TextUtils.isEmpty(login)) {
            loginView.setError("Niepoprawna nazwa użytkownika");
            focusView = loginView;
            cancel = true;
        }

        if (cancel)
            focusView.requestFocus();
        else {
            showProgress(true);
            isError = false;
            if (Connection.isOnline(this))
                Connection.postRegister(new AccountDetails(loginView.getText().toString(), mPasswordView.getText().toString()), this);
            else {
                showProgress(false);
                Toast.makeText(MainActivity.context, "Problem połączenia z serwerem", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isPasswordValid(String password) { return password.length() > 4; }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mRegisterFormView.animate()
                .setDuration(shortAnimTime)
                .alpha(show ? 0 : 1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate()
                .setDuration(shortAnimTime)
                .alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        if (!isError) {
                            Toast.makeText(MainActivity.context, "Zarejestrowano", Toast.LENGTH_SHORT).show();
                            MainActivity.userPreferences.login = loginView.getText().toString();
                            MainActivity.userPreferences.password = mPasswordView.getText().toString();
                            LocalFiles.writeUserPreferences(context);
                            finish();
                        }
                        else
                            showProgress(false);
                    }
                });
    }

    public void onBackPressed(View view) { finish(); }
}