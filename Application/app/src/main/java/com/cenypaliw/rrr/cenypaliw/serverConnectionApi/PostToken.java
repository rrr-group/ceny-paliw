package com.cenypaliw.rrr.cenypaliw.serverConnectionApi;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.cenypaliw.rrr.cenypaliw.LoginActivity;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.classes.Token;
import com.google.gson.Gson;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class PostToken extends AsyncTask<Void, Void, Token> {
    private final String TAG = "PostToken";
    private String wholeURL;

    Context context;
    String userName, password;

    @Override
    protected Token doInBackground(Void... params) {
        try {
            wholeURL = Connection.siteURL + "/token";
            URL siteEndpoint = new URL(wholeURL);
            HttpURLConnection myConnection = (HttpURLConnection) siteEndpoint.openConnection();
            return serverRequest(myConnection);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Token serverRequest(HttpURLConnection myConnection) throws IOException {
        myConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        myConnection.setRequestMethod("POST");
        OutputStream outputStream = myConnection.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStreamWriter.write("grant_type=password&userName=" + userName + "&password=" + password);
        outputStreamWriter.flush();
        outputStreamWriter.close();
        outputStream.close();
        int responseCode = myConnection.getResponseCode();
        if (responseCode == 200 || responseCode == 201) {
            return serverResponse(myConnection);
        } else {
            Log.w(TAG, "Problem połączenia z serwerem: " + responseCode);
            if (responseCode == 400) {
                serverResponseError(myConnection.getErrorStream());
            }
        }
        return null;
    }

    public void serverResponseError(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.defaultCharset()))) {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        Log.e(TAG, stringBuilder.toString());
    }

    private Token serverResponse(HttpURLConnection myConnection) throws IOException {
        InputStream responseBody = myConnection.getInputStream();
        Reader reader = new InputStreamReader(responseBody, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        return gson.fromJson(reader, Token.class);
    }

    @Override
    protected void onPostExecute(Token token) {
        super.onPostExecute(token);
        if (token == null)
            Toast.makeText(context, "Niepoprawny login lub hasło", Toast.LENGTH_SHORT).show();
        MainActivity.token = token;
        LoginActivity.mLoginFormView.animate().cancel();
        LoginActivity.mProgressView.animate().cancel();
    }
}
