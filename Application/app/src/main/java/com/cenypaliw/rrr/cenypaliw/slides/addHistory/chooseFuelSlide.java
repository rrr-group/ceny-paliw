package com.cenypaliw.rrr.cenypaliw.slides.addHistory;

import agency.tango.materialintroscreen.SlideFragment;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.cenypaliw.rrr.cenypaliw.AddHistoryActivity;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.R;

import java.util.ArrayList;
import java.util.List;

public class chooseFuelSlide extends SlideFragment {

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_choose_fuel_slide_history, container, false);
        ListView listView = view.findViewById(R.id.fuelNamesListView);
        TextView fuelTypeTextView = view.findViewById(R.id.text);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_list_item_1, getListOfFuelNames());
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener((parent, v, position, id) -> {
            position -= listView.getFirstVisiblePosition();
            AddHistoryActivity.fuelType = position + 1;
            fuelTypeTextView.setText(getString(R.string.chosen) + " " + MainActivity.fuelNames.get(position).name);
        });
        fuelTypeTextView.setText(getString(R.string.chosen) + " " + MainActivity.fuelNames.get(MainActivity.userPreferences.fuelType).name);
        return view;
    }

    @Override
    public int backgroundColor() {
        return R.color.colorPrimary;
    }

    @Override
    public int buttonsColor() { return R.color.colorGray; }

    @Override
    public boolean canMoveFurther() {
        return true;
    }

    private List<String> getListOfFuelNames()
    {
        ArrayList<String> fuelNamesList = new ArrayList<>();
        for(int i = 0; i < MainActivity.fuelNames.size(); i++) {
            fuelNamesList.add(MainActivity.fuelNames.get(i).name);
        }
        return fuelNamesList;
    }
}
