package com.cenypaliw.rrr.cenypaliw.serverConnectionApi;

import android.os.AsyncTask;
import android.util.Log;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.classes.Fuel;
import com.cenypaliw.rrr.cenypaliw.classes.Station;
import com.cenypaliw.rrr.cenypaliw.classes.Token;
import com.cenypaliw.rrr.cenypaliw.classes.UserPreferences;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes.FuelNewPriceDetails;
import com.google.gson.Gson;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class PostFuelPrice extends AsyncTask<Void, Void, Fuel>{
    private final String TAG = "GetFuelsStationId";
    private final String wholeURL = Connection.siteURL + "api/Fuels/";

    public int fuelNameId;
    public Station station;
    public double price;

    @Override
    protected Fuel doInBackground(Void... params) {
        try {
            URL siteEndpoint = new URL(wholeURL);
            HttpURLConnection myConnection = (HttpURLConnection) siteEndpoint.openConnection();
            return serverRequest(myConnection);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Fuel serverRequest(HttpURLConnection myConnection) throws IOException {
        Gson gson = new Gson();
        myConnection.setRequestProperty("Content-Type", "application/json");
        myConnection.setRequestProperty("Authorization", "Bearer " + MainActivity.token.access_token);
        myConnection.setRequestMethod("POST");
        myConnection.setDoOutput(true);
        OutputStream outputStream = myConnection.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStreamWriter.write(gson.toJson(new FuelNewPriceDetails(fuelNameId, station.stationId, price)));
        outputStreamWriter.flush();
        outputStreamWriter.close();
        outputStream.close();
        int responseCode = myConnection.getResponseCode();
        if (responseCode == 200 || responseCode == 201) {
            return serverResponse(myConnection);
        } else {
            Log.w(TAG, "Problem połączenia z serwerem: " + responseCode);
            if (responseCode == 400) {
                Connection.serverErrorResponse(myConnection, TAG);
            }
        }
        return null;
    }

    private Fuel serverResponse(HttpURLConnection myConnection) throws IOException {
        InputStream responseBody = myConnection.getInputStream();
        Reader reader = new InputStreamReader(responseBody, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        return gson.fromJson(reader, Fuel.class);
    }

    @Override
    protected void onPostExecute(Fuel fuel) {
        super.onPostExecute(fuel);
    }

}
