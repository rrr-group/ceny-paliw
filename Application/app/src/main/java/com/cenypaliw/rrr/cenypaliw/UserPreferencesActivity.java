package com.cenypaliw.rrr.cenypaliw;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import com.cenypaliw.rrr.cenypaliw.localFilesApi.LocalFiles;
import com.cenypaliw.rrr.cenypaliw.slides.userPreferences.chooseFuelSlide;
import com.cenypaliw.rrr.cenypaliw.slides.userPreferences.chooseStationRadiusSlide;
import com.cenypaliw.rrr.cenypaliw.slides.userPreferences.permissionGrantSlide;

public class UserPreferencesActivity extends MaterialIntroActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            addSlide(new permissionGrantSlide());
        addSlide(new chooseFuelSlide());
        addSlide(new chooseStationRadiusSlide());
    }

    @Override
    public void onFinish() {
        super.onFinish();
        LocalFiles.writeUserPreferences(this);
    }

    @Override
    public void onBackPressed() { }
}