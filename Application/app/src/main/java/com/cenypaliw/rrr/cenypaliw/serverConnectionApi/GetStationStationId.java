package com.cenypaliw.rrr.cenypaliw.serverConnectionApi;

import android.os.AsyncTask;
import android.util.Log;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.classes.Station;
import com.google.gson.Gson;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class GetStationStationId extends AsyncTask<Void, Void, Station> {
    private final String TAG = "GetStationStationId";
    private final String wholeURL = Connection.siteURL + "api/Stations/";

    int stationId;

    @Override
    protected Station doInBackground(Void... params) {
        try {
            URL siteEndpoint = new URL(wholeURL + stationId);
            HttpURLConnection myConnection = (HttpURLConnection) siteEndpoint.openConnection();
            return serverRequest(myConnection);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Station serverRequest(HttpURLConnection myConnection) throws IOException {
        myConnection.setRequestProperty("Content-Type", "application/json");
        myConnection.setRequestMethod("GET");
        int responseCode = myConnection.getResponseCode();
        if (responseCode == 200 || responseCode == 201) {
            return serverResponse(myConnection);
        } else {
            Log.w(TAG, "Problem połączenia z serwerem: " + responseCode);
            if (responseCode == 400) {
                Connection.serverErrorResponse(myConnection, TAG);
            }
        }
        return null;
    }

    private Station serverResponse(HttpURLConnection myConnection) throws IOException {
        InputStream responseBody = myConnection.getInputStream();
        Reader reader = new InputStreamReader(responseBody, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        return gson.fromJson(reader, Station.class);
    }

    @Override
    protected void onPostExecute(Station station) {
        super.onPostExecute(station);
        Connection.getFuelsStationId(station);
        MainActivity.stations.add(station);
    }
}