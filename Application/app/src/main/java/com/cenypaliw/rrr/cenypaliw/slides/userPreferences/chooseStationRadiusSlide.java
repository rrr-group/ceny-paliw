package com.cenypaliw.rrr.cenypaliw.slides.userPreferences;

import agency.tango.materialintroscreen.SlideFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.R;
import me.tankery.lib.circularseekbar.CircularSeekBar;

public class chooseStationRadiusSlide extends SlideFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_choose_station_radius, container, false);
        CircularSeekBar seekBar = view.findViewById(R.id.seekbar);
        TextView radiusTextView = view.findViewById(R.id.radius);
        seekBar.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, float progress, boolean fromUser) {
                radiusTextView.setText((int)progress + " km");
                MainActivity.userPreferences.radius = (int)progress * 1000;
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {

            }
        });
        return view;
    }

    @Override
    public int backgroundColor() {
        return R.color.colorPrimary;
    }

    @Override
    public int buttonsColor() {
        return R.color.colorInputs;
    }

    @Override
    public boolean canMoveFurther() { return MainActivity.userPreferences.radius > 0; }

    @Override
    public String cantMoveFurtherErrorMessage() { return "Odległość wyszukiwania stacji musi być większa od 0"; }
}
