package com.cenypaliw.rrr.cenypaliw;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.cenypaliw.rrr.cenypaliw.classes.History;
import com.cenypaliw.rrr.cenypaliw.localFilesApi.LocalFiles;

public class FuelConsumptionCalculatorActivity extends AppCompatActivity {
    private EditText fuelConsumptionEditText, distanceEditText, fuelPriceEditText;
    private Float fuel, distance, fuelPrice, averageConsumption, cost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LocalFiles.readHistory(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_consumption_calculator);
        fuelConsumptionEditText = findViewById(R.id.fuelConsumption);
        distanceEditText = findViewById(R.id.distance);
        fuelPriceEditText = findViewById(R.id.fuelPrice);
        if (!RefuelingHistoryActivity.history.isEmpty())
        {
            float avgPaid = 0, avgLiters = 0, avgOdometer = 0, lastodometer = -1;
            for (History obj : RefuelingHistoryActivity.history) {
                avgPaid += obj.paid;
                avgLiters += obj.liters;
                if (lastodometer < 0)
                    lastodometer = obj.odometer;
                else
                    avgOdometer += Math.abs(obj.odometer - lastodometer);
            }
            if (RefuelingHistoryActivity.history.size() > 1) {
                if (avgOdometer <= 0)
                    avgOdometer = 1;
                fuelPrice = avgPaid / avgLiters;
                averageConsumption = avgLiters / (avgOdometer / 100);
                cost = avgOdometer / 100 * averageConsumption * fuelPrice;
                fuelConsumptionEditText.setText(String.format("%.02f", avgLiters));
                distanceEditText.setText(String.format("%.02f", avgOdometer));
                fuelPriceEditText.setText(String.format("%.02f", fuelPrice));
                setResultText();
            }
        }
    }

    public void calculate(View v) {
        if (checkData()) {
            averageConsumption = fuel / (distance / 100);
            cost = distance / 100 * averageConsumption * fuelPrice;
            setResultText();
        }
    }

    private void setResultText() {
        TextView resultTextView = findViewById(R.id.resultTitle);
        resultTextView.setText(getResultText());
    }

    private String getResultText() {
        return getString(R.string.consumption)
                + "\n" + String.format("%.02f", averageConsumption) + " "
                + getString(R.string.per100km)
                + "\n" + "\n"
                + getString(R.string.cost_of_trace)
                + "\n" + String.format("%.02f", cost)
                + getString(R.string.currency_symbol);
    }

    private boolean checkData() {
        if (!fieldsIsEmpty()) {
            fuel = Float.valueOf(fuelConsumptionEditText.getText().toString());
            distance = Float.valueOf(distanceEditText.getText().toString());
            fuelPrice = Float.valueOf(fuelPriceEditText.getText().toString());
            return isValuesInRange();
        }
        return false;
    }

    private boolean fieldsIsEmpty() {
        EditText temp = null;
        if (TextUtils.isEmpty(fuelConsumptionEditText.getText().toString()))
            temp = fuelConsumptionEditText;
        else if (TextUtils.isEmpty(distanceEditText.getText().toString()))
            temp = distanceEditText;
        else if (TextUtils.isEmpty(fuelPriceEditText.getText().toString()))
            temp = fuelPriceEditText;
        if (temp != null) {
            temp.setError(getString(R.string.enter_data));
            temp.requestFocus();
            return true;
        }
        return false;
    }

    private boolean isValuesInRange() {
        EditText temp = null;
        if (fuel <= 0 || fuel > 1000)
            temp = fuelConsumptionEditText;
        else if (distance <= 0 || distance > 50000)
            temp = distanceEditText;
        else if (fuelPrice < 1 && fuelPrice > 10)
            temp = fuelPriceEditText;
        if (temp != null) {
            temp.setError(getString(R.string.wrong_value_error_message));
            temp.requestFocus();
            return false;
        }
        return true;
    }

    public void onBackPressed(View view) { finish(); }
}
