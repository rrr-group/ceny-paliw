package com.cenypaliw.rrr.cenypaliw.classes;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class Station {
    public int stationId;
    public String name, description, addedByUser, approvedByUser;
    public boolean isActive, isApproved;
    public double latitude, longitude;
    public ArrayList<Fuel> fuels;

    public LatLng getPosition() {
        return new LatLng(latitude, longitude);
    }

    public Fuel getFuelByFuelNameId(int fuelNameId)
    {
        for(int i = 0; i < fuels.size(); i++) {
            if (fuels.get(i).fuelNameId == fuelNameId) {
                return fuels.get(i);
            }
        }
        return fuels.get(0);
    }
}
