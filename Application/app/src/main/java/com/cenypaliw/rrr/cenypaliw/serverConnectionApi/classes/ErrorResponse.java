package com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes;

import android.util.Log;

public class ErrorResponse {
    public int Code;
    public String Message, AdditionalErrors, Timestamp, Guid;

    public void log (String TAG) {
        Log.println(Log.ASSERT, TAG, Code + Message + "\n" + AdditionalErrors + "\n" + Timestamp + "\n" + Guid);
    }
}
