package com.cenypaliw.rrr.cenypaliw.classes;

public class Token {
    public String access_token, token_type;
    public int expires_in;

    public Token(String access_token, String token_type, int expires_in) {
        this.access_token = access_token;
        this.token_type = token_type;
        this.expires_in = expires_in;
    }
}
