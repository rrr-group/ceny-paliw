package com.cenypaliw.rrr.cenypaliw;

import android.annotation.SuppressLint;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.cenypaliw.rrr.cenypaliw.localFilesApi.LocalFiles;
import com.github.amlcurran.showcaseview.ShowcaseView;

public class IntroductionActivity extends AppCompatActivity {
    private ShowcaseView sv;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);
        sv = new ShowcaseView.Builder(this)
                .setTarget(() -> {
                    Point point = new Point();
                    point.set(500, 2500);
                    return point;
                })
                .withNewStyleShowcase()
                .setContentTitle("Witamy w aplikacji Ceny Paliw")
                .setContentText("Kliknij gdziekolwiek, aby dowiedzieć się więcej na temat użytkowania aplikacji.")
                .build();
        sv.hideButton();
        sv.setOnTouchListener((v, event) -> {
            sv.setAlpha(0);
            nextEvent((int) event.getX(), (int) event.getY());
            return false;
        });
    }

    private void nextEvent(int x, int y) {
        if (x < 250 && y < 250)
            menuButton();
        else if (x < 250 && y < 500)
            refreshButton();
        else if (x >= 940 && y <= 200)
            showMyLocationButton();
        else if (x >= 170 && y >= 880 && x <= 940 && y <= 1240)
            infoWindow();
        else if (x >= 780 && y >= 670 && x <= 880 && y <= 800)
            upperMarker();
        else if (x >= 850 && y >= 1400 && x <= 940 && y <= 1500)
            lowerMarker();
        else if (x <= 250 && y >= 1750)
            logoGoogle();
        else if (x >= 850 && y >= 1750)
            googleMapsButtons();
        else if (x >= 680 && y >= 350 && x <= 780 && y <= 450)
            myLocation();
        else if (x > 250 && y >= 1700 && x < 850) {
            MainActivity.userPreferences.showedIntroduction = true;
            LocalFiles.writeUserPreferences(this);
            finish();
        }
    }

    private void menuButton() {
        sv.setTarget(() -> {
            Point point = new Point();
            point.set(145, 175);
            return point;
        });
        sv.setContentTitle("Menu");
        sv.setContentText("Po wciśnięciu pojawi się menu z dodatkowymi opcjami:\n- kalkulator spalania\n- historia tankowań\n- ekran wprowadzenia (właśnie tu teraz jesteś)");
        sv.show();
    }

    private void refreshButton() {
        sv.setTarget(() -> {
            Point point = new Point();
            point.set(145, 450);
            return point;
        });
        sv.setContentTitle("Odśwież filtry");
        sv.setContentText("Po wciśnięciu pojawi się opcja wybrania preferowanego paliwa oraz zasięgu wyświetlania stacji.");
        sv.show();
    }

    private void showMyLocationButton() {
        sv.setTarget(() -> {
            Point point = new Point();
            point.set(1010, 125);
            return point;
        });
        sv.setContentTitle("Moja lokalizacja");
        sv.setContentText("Po wciśnięciu aplikacja będzie pokazywać oraz przesuwać mapę do aktualnej lokalizacji.");
        sv.show();
    }

    private void infoWindow() {
        sv.setTarget(() -> {
            Point point = new Point();
            point.set(540, 1140);
            return point;
        });
        sv.setContentTitle("Informacje o stacji");
        sv.setContentText("Po kliknięciu na znacznik na mapie ukaże się menu z trzema przyciskami:\n- dodawanie cen paliw\n- dodawanie do ulubionych\n- informacje o stacji");
        sv.show();
    }

    private void upperMarker() {
        sv.setTarget(() -> {
            Point point = new Point();
            point.set(850, 750);
            return point;
        });
        sv.setContentTitle("Znacznik");
        sv.setContentText("Po kliknięciu pojawi się dymek z informacjami o nazwie stacji, cenie paliwa oraz trzema przyciskami. Kolor żółty oznacza znacznik dodany do ulubionych.");
        sv.show();
    }

    private void lowerMarker() {
        sv.setTarget(() -> {
            Point point = new Point();
            point.set(900, 1500);
            return point;
        });
        sv.setContentTitle("Znacznik");
        sv.setContentText("Po kliknięciu pojawi się dymek z informacjami o nazwie stacji, cenie paliwa oraz trzema przyciskami. Kolor żółty oznacza znacznik dodany do ulubionych.");
        sv.show();
    }

    private void logoGoogle() {
        sv.setTarget(() -> {
            Point point = new Point();
            point.set(135, 1835);
            return point;
        });
        sv.setContentTitle("Google");
        sv.setContentText("Logo firmy, która udostępnia mapy dla tej aplikacji");
        sv.show();
    }

    private void googleMapsButtons() {
        sv.setTarget(() -> {
            Point point = new Point();
            point.set(900, 1800);
            return point;
        });
        sv.setContentTitle("Google Maps");
        sv.setContentText("Po kliknięciu włączy w lewy przycisk aplikacja włączy mapy google i będzie prowadziła do stacji, natomiast prawy tylko włączy mapy google");
        sv.show();
    }

    private void myLocation() {
        sv.setTarget(() -> {
            Point point = new Point();
            point.set(760, 430);
            return point;
        });
        sv.setContentTitle("Moja lokalizacja na mapie");
        sv.setContentText("Po kliknięciu pokaże informację o szerokości i wysokości geograficznej.");
        sv.show();
    }
}
