package com.cenypaliw.rrr.cenypaliw;

import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.Toast;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.Connection;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class AddStationActivity extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private LatLng position;
    private String stationName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_station);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Marker marker = mMap.addMarker(new MarkerOptions().position(MainActivity.myPosition).flat(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(MainActivity.myPosition));
        mMap.animateCamera(CameraUpdateFactory.zoomTo( 15.0f ));
        mMap.setOnMapClickListener(position -> {
            marker.setPosition(position);
            mMap.animateCamera(CameraUpdateFactory.newLatLng(position));
            this.position = position;
        });
        position = MainActivity.myPosition;
    }

    public void addStation() {
        if (stationName != null) {
            Connection.postStationsInsert(stationName, position);
            Toast.makeText(this, "Dodano stację", Toast.LENGTH_LONG).show();
        }
        finish();
    }

    public void onBackPressed(View view) {
        super.onBackPressed();
        finish();
    }

    public void showDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Wybierz nazwę stacji");
        String[] names = {"Lotos", "PKN Orlen", "Bliska", "Shell", "BP"};
        builder.setItems(names, (dialog, which) -> stationName = names[which]);
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(R.color.colorPrimary);
        dialog.setOnDismissListener(v -> addStation());
    }


}
