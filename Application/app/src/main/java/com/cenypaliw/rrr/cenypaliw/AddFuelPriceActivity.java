package com.cenypaliw.rrr.cenypaliw;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import com.cenypaliw.rrr.cenypaliw.classes.Station;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.Connection;

import java.util.ArrayList;
import java.util.List;

public class AddFuelPriceActivity extends AppCompatActivity {
    private int fuelNameId;
    private Station station;
    private EditText price;
    private TextInputLayout textInputLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_fuel_price);
        TextView addPriceResult = findViewById(R.id.addPriceResult);
        ListView addPriceFuelList = findViewById(R.id.addPriceFuelList);
        price = findViewById(R.id.addPriceEditText);
        textInputLayout = findViewById(R.id.addPriceTextInputLayout);
        station = MainActivity.stations.get(MainActivity.clickedMarkerID);
        fuelNameId = MainActivity.userPreferences.fuelType;
        addPriceResult.setText("Ostatnia cena wybranego paliwa\n" + MainActivity.getFuelNameByFuelNameId(fuelNameId) + ": " + station.getFuelByFuelNameId(fuelNameId).price + " zł");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getListOfFuels());
        addPriceFuelList.setAdapter(arrayAdapter);
        addPriceFuelList.setOnItemClickListener((parent, view, position, id) -> {
            fuelNameId = station.fuels.get(position).fuelNameId;
            addPriceResult.setText("Ostatnia cena wybranego paliwa\n" + MainActivity.getFuelNameByFuelNameId(fuelNameId) + ": " + station.fuels.get(position).price + " zł");
        });
    }

    private List<String> getListOfFuels()
    {
        ArrayList<String> fuelsList = new ArrayList<>();
        for(int i = 0; i < station.fuels.size(); i++)
            fuelsList.add(MainActivity.getFuelNameByFuelNameId(station.fuels.get(i).fuelNameId));
        return fuelsList;
    }

    public void addPrice(View view) {
        if (!TextUtils.isEmpty(price.getText().toString())) {
            Double sendPrice = Double.parseDouble(price.getText().toString());
            if (sendPrice < 10 && sendPrice > 0) {
                Connection.postFuels(fuelNameId, station, sendPrice);
                Toast.makeText(this, "Dodano cenę paliwa", Toast.LENGTH_SHORT).show();
                finish();
            }
            else
                textInputLayout.setError("Wpisz prawdziwą cenę paliwa");
        }
        else
            textInputLayout.setError("Wpisz cenę paliwa");

    }

    public void onBackPressed(View view) {
        finish();
    }


}