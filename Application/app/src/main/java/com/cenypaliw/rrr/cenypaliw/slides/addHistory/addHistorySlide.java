package com.cenypaliw.rrr.cenypaliw.slides.addHistory;

import agency.tango.materialintroscreen.SlideFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.cenypaliw.rrr.cenypaliw.AddHistoryActivity;
import com.cenypaliw.rrr.cenypaliw.R;

public class addHistorySlide extends SlideFragment {
    private EditText litersEditText, paidEditText, dateEditText, odometerEditText;
    private boolean onCreate = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_add_history_slide, container, false);
        litersEditText = view.findViewById(R.id.litersHistory);
        paidEditText = view.findViewById(R.id.paidHistory);
        dateEditText = view.findViewById(R.id.dateHistory);
        odometerEditText = view.findViewById(R.id.odometerHistory);
        return view;
    }

    @Override
    public int backgroundColor() { return R.color.colorPrimary; }

    @Override
    public int buttonsColor() { return R.color.colorGray; }

    @Override
    public boolean canMoveFurther() {
        if (onCreate) {
            onCreate = false;
            return false;
        }
        return checkData();
    }

    private boolean checkData() {
        if (!fieldsIsEmpty()) {
            AddHistoryActivity.liters = Double.valueOf(litersEditText.getText().toString());
            AddHistoryActivity.paid = Double.valueOf(paidEditText.getText().toString());
            AddHistoryActivity.date = dateEditText.getText().toString();
            AddHistoryActivity.odometer = Integer.valueOf(odometerEditText.getText().toString());
            return isValuesInRange();
        }
        return false;
    }

    private boolean fieldsIsEmpty() {
        EditText temp = null;
        if (TextUtils.isEmpty(litersEditText.getText().toString()))
            temp = litersEditText;
        else if (TextUtils.isEmpty(paidEditText.getText().toString()))
            temp = paidEditText;
        else if (TextUtils.isEmpty(odometerEditText.getText().toString()))
            temp = odometerEditText;
        if (temp != null) {
            temp.setError(getString(R.string.enter_data));
            temp.requestFocus();
            return true;
        }
        if (TextUtils.isEmpty(dateEditText.getText().toString())) {
            dateEditText.setError(getString(R.string.choose_date));
            dateEditText.requestFocus();
            return true;
        }
        return false;
    }

    private boolean isValuesInRange() {
        EditText temp = null;
        if (AddHistoryActivity.liters <= 0 || AddHistoryActivity.liters > 1000)
            temp = litersEditText;
        else if (AddHistoryActivity.paid <= 0 || AddHistoryActivity.liters > 50000)
            temp = paidEditText;
        else if (AddHistoryActivity.odometer <= 0)
            temp = odometerEditText;
        if (temp != null) {
            temp.setError(getString(R.string.wrong_value_error_message));
            temp.requestFocus();
            return false;
        }
        return true;
    }
}
