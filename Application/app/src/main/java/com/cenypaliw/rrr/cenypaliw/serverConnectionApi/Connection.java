package com.cenypaliw.rrr.cenypaliw.serverConnectionApi;

import android.content.Context;
import com.cenypaliw.rrr.cenypaliw.MainActivity;
import com.cenypaliw.rrr.cenypaliw.classes.Station;
import com.cenypaliw.rrr.cenypaliw.localFilesApi.LocalFiles;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes.AccountDetails;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes.ErrorResponse;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.classes.StationDetails;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;

public class Connection {
    static final String pingURL = "rrrgroup-001-site1.htempurl.com";
    static final String siteURL = "http://" + pingURL + "/";

    public static void getResources(LatLng myPosiiton, int radius, Context context) {
        postStations(myPosiiton, radius);
        getFuelsNames(context);
    }

    public static void getFuelsNames(Context context) {
        if (MainActivity.fuelNames == null) {
            LocalFiles.readFuelNames(context);
            if (MainActivity.fuelNames == null) {
                GetFuelsNames conn = new GetFuelsNames();
                conn.execute();
            }
        }
    }

    public static void postRegister(AccountDetails account, Context context) {
        PostRegister conn = new PostRegister();
        conn.account = account;
        conn.context = context;
        conn.execute();
    }

    private static void postStations(LatLng myPosiiton, int radius) {
        PostStations conn = new PostStations();
        conn.myPosition = myPosiiton;
        conn.radius = radius;
        conn.execute();
    }

    public static void postStationsInsert(String Name, LatLng position) {
        PostStationsInsert conn = new PostStationsInsert();
        conn.newStation = new StationDetails(Name, position);
        conn.execute();
    }

    public static void postToken(String userName, String password, Context context) {
        PostToken conn = new PostToken();
        conn.context = context;
        conn.userName = userName.replace("@", "A");
        conn.password = password;
        conn.execute();
    }

    static void getStationStationId(int stationId) {
        GetStationStationId conn = new GetStationStationId();
        conn.stationId = stationId;
        conn.execute();
    }

    static void getFuelsStationId(Station station) {
        GetFuelsStationId conn = new GetFuelsStationId();
        conn.station = station;
        conn.execute();
    }

    public static void postFuels(int fuelNameId, Station station, double price) {
        PostFuelPrice conn = new PostFuelPrice();
        conn.fuelNameId = fuelNameId;
        conn.station = station;
        conn.price = price;
        conn.execute();
    }

    static void serverErrorResponse(HttpURLConnection myConnection, String TAG) {
        InputStream responseBody = myConnection.getErrorStream();
        Reader reader = new InputStreamReader(responseBody, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        ErrorResponse errorResponse = gson.fromJson(reader, ErrorResponse.class);
        errorResponse.log(TAG);
    }

    public static boolean isOnline(Context context) {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 " + pingURL);
            int exitValue = ipProcess.waitFor();
            if (exitValue == 0) {
                if (MainActivity.fuelNames == null)
                    Connection.getFuelsNames(context);
                return true;
            }
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }
        return false;
    }
}