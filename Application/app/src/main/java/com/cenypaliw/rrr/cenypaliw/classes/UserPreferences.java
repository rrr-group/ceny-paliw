package com.cenypaliw.rrr.cenypaliw.classes;

public class UserPreferences {
    public int fuelType = -1, radius = 0;
    public String login = "empty", password = "empty";
    public boolean showedIntroduction = false;

    public void setDefault() {
        fuelType = -1;
        radius = 0;
    }
}
