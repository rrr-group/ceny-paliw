package com.cenypaliw.rrr.cenypaliw;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.cenypaliw.rrr.cenypaliw.localFilesApi.LocalFiles;
import com.cenypaliw.rrr.cenypaliw.serverConnectionApi.Connection;

public class LoginActivity extends AppCompatActivity {
    public static View mProgressView;
    public static View mLoginFormView;
    public static Button skipLoginButton;

    private static Context context;

    private EditText loginView;
    private EditText mPasswordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.context = this;
        TextView registerLink = findViewById(R.id.registerLink);
        registerLink.setPaintFlags(registerLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        loginView = findViewById(R.id.login);
        mPasswordView = findViewById(R.id.password);
        findViewById(R.id.login_button).setOnClickListener(view -> attemptLogin());
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        skipLoginButton = findViewById(R.id.skip_login_button);
        skipLoginButton.setVisibility(View.GONE);
        if (Connection.isOnline(this)) {
            Connection.getFuelsNames(this);
            skipLoginButton.setVisibility(View.VISIBLE);
            if (!MainActivity.userPreferences.login.equals("empty") && !MainActivity.userPreferences.password.equals("empty")) {
                loginView.setText(MainActivity.userPreferences.login);
                mPasswordView.setText(MainActivity.userPreferences.password);
                attemptLogin();
            }
        }
        else
            Toast.makeText(MainActivity.context, "Problem połączenia z serwerem", Toast.LENGTH_LONG).show();
    }

    private void attemptLogin() {
        loginView.setError(null);
        mPasswordView.setError(null);

        String login = loginView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError("Niepoprawne hasło");
            focusView = mPasswordView;
            cancel = true;
        }
        if (TextUtils.isEmpty(login)) {
            loginView.setError("Niepoprawna nazwa użytkownika");
            focusView = loginView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            if (Connection.isOnline(this)) {
                skipLoginButton.setVisibility(View.VISIBLE);
                Connection.postToken(login, password, this);
            }
            else {
                showProgress(false);
                Toast.makeText(MainActivity.context, "Problem połączenia z serwerem", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4; //TODO: password validation
    }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate()
                .setDuration(shortAnimTime)
                .alpha(show ? 0 : 1)
                .setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate()
                .setDuration(shortAnimTime)
                .alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                if (MainActivity.token != null) {
                    Toast.makeText(MainActivity.context, "Zalogowano", Toast.LENGTH_SHORT).show();
                    MainActivity.userPreferences.login = loginView.getText().toString();
                    MainActivity.userPreferences.password = mPasswordView.getText().toString();
                    LocalFiles.writeUserPreferences(context);
                    finish();
                }
                else {
                    showProgress(false);
                }
            }
        });
    }

    public void onRegisterButtonClick(View view) { startActivity(new Intent(this, RegisterActivity.class)); }

    public void onSkipLoginButtonClick(View view) {
        if (MainActivity.fuelNames != null && !MainActivity.fuelNames.isEmpty()) {
            MainActivity.isLoginSkipped = true;
            finish();
        }
        else
            Toast.makeText(this, "Nie pobrano nazw paliw spróbuj ponownie za chwilę", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!MainActivity.userPreferences.login.equals("empty") && !MainActivity.userPreferences.password.equals("empty")) {
            loginView.setText(MainActivity.userPreferences.login);
            mPasswordView.setText(MainActivity.userPreferences.password);
            attemptLogin();
        }
    }

    @Override
    public void onBackPressed() {
        if ((MainActivity.isLoginSkipped || MainActivity.token != null))
            finish();
    }
}